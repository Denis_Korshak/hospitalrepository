package by.epam.javatraining.korshak.hospital.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.entity.role.PatientRole;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.PatientService;
import by.epam.javatraining.korshak.hospital.service.impl.PatientServiceImpl;

public class FinishInspectionCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		PatientService patientService = new PatientServiceImpl();
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null || !user.isDoctor()){
			return ResourceManager.getProperty("page.index");
		}
		
		Patient patient = (Patient)request.getSession().getAttribute("patient");
		String selectedId = request.getParameter("selectedId");
		if(selectedId != null){
			patient = patientService.read(Integer.parseInt(selectedId));
			request.getSession().setAttribute("patient", patient);
		}
		if(null == patient){
			return ResourceManager.getProperty("page.patients");
		}
		
		patient.setDoctorId(user.getStaffId());
		patient.setStatus(PatientRole.TREATED);
		patientService.update(patient);
		
		return (new ShowNewPatientsCommand()).execute(request);
	}

}
