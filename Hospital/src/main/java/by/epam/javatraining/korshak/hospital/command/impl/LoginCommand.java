package by.epam.javatraining.korshak.hospital.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.LoginService;
import by.epam.javatraining.korshak.hospital.service.StaffService;
import by.epam.javatraining.korshak.hospital.service.impl.LoginServiceImpl;
import by.epam.javatraining.korshak.hospital.service.impl.StaffServiceImpl;
import static by.epam.javatraining.korshak.hospital.paramname.ParamName.*;

public class LoginCommand implements ActionCommand{
	
	@Override
	public String execute(HttpServletRequest request) throws ServiceException{
		String login = request.getParameter(LOGIN);
		String password = request.getParameter(PASSWORD);
		LoginService loginService = new LoginServiceImpl();
		int userId = loginService.loginUser(login, password);
		if( userId != -1){
			StaffService staffService = new StaffServiceImpl();
			Staff staff = staffService.read(userId);
			request.getSession().setAttribute("user", staff);
		} else {
			request.setAttribute("message", "Please enter something!!!!"); 
		}
		return ResourceManager.getProperty("page.index");
		
	}

}
