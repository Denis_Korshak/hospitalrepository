package by.epam.javatraining.korshak.hospital.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.AppointmentRecord;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.AppointmentRecordService;
import by.epam.javatraining.korshak.hospital.service.impl.AppointmentRecordServiceImpl;

public class PerfomeAppointmentCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		AppointmentRecordService appointmentRecordService = new AppointmentRecordServiceImpl();
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null || (!user.isDoctor() && !user.isNurse())){
			return ResourceManager.getProperty("page.index");
		}
		
		String selectedId = request.getParameter("selectedId");
		
		AppointmentRecord appointmentRecord = appointmentRecordService.read(Integer.parseInt(selectedId));
		appointmentRecord.setPerfomedStaffId(user.getStaffId());
		appointmentRecordService.update(appointmentRecord);
		
		return (new ShowAppointmentsOnToday()).execute(request);
	}

}
