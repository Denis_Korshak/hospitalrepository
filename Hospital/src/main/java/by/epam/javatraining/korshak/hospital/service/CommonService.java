package by.epam.javatraining.korshak.hospital.service;

import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public interface CommonService<T> {
	
	boolean create(T entity) throws ServiceException;
	
	void update(T entity) throws ServiceException;
	
	T read(int id) throws ServiceException;

	void delete(int id) throws ServiceException;
}
