package by.epam.javatraining.korshak.hospital.customtag;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import by.epam.javatraining.korshak.hospital.entity.Patient;

@SuppressWarnings("serial")
public class TableTag extends TagSupport {
	
	private ArrayList collection;
	private int rowsPerPage = Integer.MAX_VALUE;
	private int currentPage = 1;
	
	private int rowNumber = 0;
	private Object currentElement;
	
	public ArrayList getCollection() {
		return collection;
	}
	public void setCollection(ArrayList collection) {
		this.collection = collection;
	}
	public int getRowsPerPage() {
		return rowsPerPage;
	}
	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		if(currentPage>0){
			this.currentPage = currentPage;
		} else {
			this.currentPage = 1;
		}
	}
	
	protected boolean isFirstIteration(){
		return rowNumber == 0;
	}
	
	protected Object getElement(){
		return currentElement;
	}
	
	public int doStartTag() throws JspTagException {
		try{
			rowNumber = 0;
			JspWriter out = pageContext.getOut();
			out.write("<table border='1' class='table table-hover active'><thead><tr>");
		} catch (IOException e) {
			throw new JspTagException(e.getMessage());
		}
		if(rowNumber<=collection.size()){
			return EVAL_BODY_INCLUDE;
		} else {
			return SKIP_BODY;
		}
	}
	
	
	public int doAfterBody() throws JspTagException{
		if(rowsPerPage - rowNumber > 0 && collection.size()>rowNumber+(currentPage-1)*rowsPerPage){
			try {
				if(isFirstIteration()){
					pageContext.getOut().write("</tr></thead><tbody><tr>");
				} else {
					pageContext.getOut().write("</tr><tr>");
				}
				rowNumber++;
			} catch (IOException e) {
				throw new JspTagException(e.getMessage());
			}
			currentElement = collection.get(rowNumber+(currentPage-1)*(rowsPerPage)-1);
			return EVAL_BODY_AGAIN;
		} else {
			
			return SKIP_BODY;
		}
	}
	
	public int doEndTag() throws JspTagException{
		try{
			pageContext.getOut().write("</tr></tbody></table>");
			writeNavigation();
		} catch(IOException e){
			throw new JspTagException(e.getMessage());
		}
		return EVAL_PAGE;
	}
	
	private void writeNavigation() throws JspTagException {
		double pages = 1;
		if(rowsPerPage != 0){
			pages = Math.ceil((double)collection.size()/rowsPerPage);
		}
		JspWriter out = pageContext.getOut();
		for(int i = 1; i <= pages; i++){
			try {
				String command = pageContext.getRequest().getParameter("command");
				out.write("<a href='controller?command="+command+"&currentpage="+i+"&rowsperpage="+rowsPerPage+"'>"+i+"</a>");
			} catch (IOException e) {
				throw new JspTagException(e.getMessage());
			}
		}
	}

}
