package by.epam.javatraining.korshak.hospital.service;

import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public interface PatientService extends CommonService<Patient>{

	List<Patient> fetchPatientsOnDepartment(int id) throws ServiceException;
	List<Patient> fetchAllPatients() throws ServiceException;
	List<Patient> fetchNewPatients(int departmentId) throws ServiceException;
	List<Patient> searchOnSurname(String surname) throws ServiceException;
	void addDiagnosis(int patientId, int diagnosisId) throws ServiceException;
	List<Diagnosis> fetchDiagnosis(int patientId) throws ServiceException;
	void deleteDiagnosis(int patientId, int diagnosisId) throws ServiceException;
	void takePatientOut(Patient patient, int diagnosisId) throws ServiceException;
}
