package by.epam.javatraining.korshak.hospital.paramname;

public class ParamName {

	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";
	public static final String SURNAME = "surname";
	public static final String NAME = "name";
	public static final String PATRONYMIC = "patronymic";
	public static final String DEPARTMENT = "department";
	public static final String DATE_OF_BIRTH = "dateOfBirth";
	public static final String WARD = "ward";
}
