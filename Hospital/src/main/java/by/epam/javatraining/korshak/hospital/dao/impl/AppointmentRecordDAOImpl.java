package by.epam.javatraining.korshak.hospital.dao.impl;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.AppointmentRecordDAO;
import by.epam.javatraining.korshak.hospital.entity.AppointmentRecord;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.poolconnections.PoolConnections;

public class AppointmentRecordDAOImpl implements AppointmentRecordDAO {

	private static final String SQL_CREATE_APPOINTMENT = 
			"INSERT INTO APPOINTMENTS(ID_PATIENT, ID_DOCTOR, ID_APPOINTMENT_LIST, "
			+"APPOINTMENT_DATETIME, ID_PERFOMED_STAFF, DOSAGE) "
			+ "VALUES(?,?,?,?,?,?)";
	
	private static final String SQL_FETCH_APPOINTMENT_ON_ID = 
			"SELECT ID_APPOINTMENT, ID_PATIENT, ID_DOCTOR, "
			+"ID_APPOINTMENT_LIST, APPOINTMENT_DATETIME, ID_PERFOMED_STAFF, DOSAGE "
			+ "from APPOINTMENTS where ID_APPOINTMENT = ?";
	
	private static final String SQL_UPDATE_APPOINTMENT = 
			"UPDATE APPOINTMENTS SET "
			+ "ID_PATIENT = ?, "
			+ "ID_DOCTOR = ?, "
			+ "ID_APPOINTMENT_LIST = ?, "
			+ "APPOINTMENT_DATETIME = ?, "
			+ "ID_PERFOMED_STAFF = ?, "
			+ "DOSAGE = ? WHERE ID_APPOINTMENT = ?";
	
	private static final String SQL_DELETE_APPOINTMENT = 
			"DELETE FROM APPOINTMENTS WHERE ID_APPOINTMENT = ?";
	
	private static final String SQL_FETCH_APPOINTMENT_ON_DATE = 
			"SELECT ID_APPOINTMENT, ID_PATIENT, ID_DOCTOR, "
			+"ID_APPOINTMENT_LIST, APPOINTMENT_DATETIME, ID_PERFOMED_STAFF, DOSAGE "
			+ "from APPOINTMENTS where APPOINTMENT_DATETIME = ?";
	
	private static final String SQL_FETCH_APPOINTMENT_ON_PATIENTID = 
			"SELECT ID_APPOINTMENT, ID_PATIENT, ID_DOCTOR, "
			+"ID_APPOINTMENT_LIST, APPOINTMENT_DATETIME, ID_PERFOMED_STAFF, DOSAGE "
			+ "from APPOINTMENTS where ID_PATIENT = ?";
	
	private static final String SQL_DELETE_ALL_APPOINTMENTS_ON_ID = 
			"DELETE FROM APPOINTMENTS WHERE ID_APPOINTMENT_LIST = ?";
	
	private static final String SQL_FETCH_OPERATIONS_ON_TODAY =
			"select a.ID_APPOINTMENT, a.ID_PATIENT, a.ID_DOCTOR, "
			+"a.ID_APPOINTMENT_LIST, a.APPOINTMENT_DATETIME, a.ID_PERFOMED_STAFF, a.DOSAGE "
			+"from (appointments a, appointments_list al) left join patients p on a.id_patient = p.id_patient "
			+ "where al.id_appointment_list = a.id_appointment_list and "
			+ "al.appointment_type = 'operation' "
			+ "and DATE(a.appointment_datetime) = CURDATE() and p.id_department = ? and id_perfomed_staff is null";
	
	private static final String SQL_FETCH_APPOINTMENTS_ON_TODAY =
			"select a.ID_APPOINTMENT, a.ID_PATIENT, a.ID_DOCTOR, "
			+ "a.ID_APPOINTMENT_LIST, a.APPOINTMENT_DATETIME, a.ID_PERFOMED_STAFF, a.DOSAGE " 
			+ "from (appointments a, appointments_list al) left join patients p on a.id_patient = p.id_patient "
			+ "where al.id_appointment_list = a.id_appointment_list and "
			+ "(al.appointment_type = 'procedure' or al.appointment_type = 'medication') "
			+ "and DATE(a.appointment_datetime) = CURDATE() and p.id_department = ? and id_perfomed_staff is null";
	
	@Override
	public boolean create(AppointmentRecord entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_CREATE_APPOINTMENT,Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, entity.getPatientId());
			preparedStatement.setInt(2, entity.getDoctorId());		
			preparedStatement.setInt(3, entity.getAppointmentId());
			if(entity.getDate()!=null){
				preparedStatement.setTimestamp(4, new Timestamp(entity.getDate().getTime()));
			} else {
				preparedStatement.setNull(4, java.sql.Types.DATE);
			}
			if(entity.getPerfomedStaffId()>0){
				preparedStatement.setInt(5, entity.getPerfomedStaffId());
			} else {
				preparedStatement.setNull(5, java.sql.Types.INTEGER);
			}
			preparedStatement.setFloat(6, entity.getDosage());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			
			return resultSet.next();
		} catch(SQLException e){
			throw new DAOException("Fail insert into Appointments.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public AppointmentRecord fetch(int id) throws DAOException {
		AppointmentRecord appointment = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_APPOINTMENT_ON_ID);
			preparedStatement.setInt(1,id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				appointment = getAppointmentFromResultSet(resultSet);
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return appointment;
	}

	@Override
	public void update(AppointmentRecord entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_UPDATE_APPOINTMENT);
			preparedStatement.setInt(1, entity.getPatientId());
			preparedStatement.setInt(2, entity.getDoctorId());		
			preparedStatement.setInt(3, entity.getAppointmentId());
			if(entity.getDate()!=null){
				preparedStatement.setTimestamp(4, new Timestamp(entity.getDate().getTime()));
			} else {
				preparedStatement.setNull(4, java.sql.Types.DATE);
			}
			if(entity.getPerfomedStaffId()>0){
				preparedStatement.setInt(5, entity.getPerfomedStaffId());
			} else {
				preparedStatement.setNull(5, java.sql.Types.INTEGER);
			}
			preparedStatement.setFloat(6, entity.getDosage());
			preparedStatement.setInt(7, entity.getAppointmentRecordId());
			preparedStatement.executeUpdate();
		} catch(SQLException e){
			throw new DAOException("Fail update Appointment " + entity,e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void delete(int id) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_DELETE_APPOINTMENT);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
		} catch(SQLException e){
			throw new DAOException("Fail delete Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void close(Statement st) throws DAOException {
		try{
			if(st != null){
				st.close();
			}
		} catch(SQLException e){
			throw new DAOException("Can't close statement.",e);
		}
	}

	@Override
	public List<AppointmentRecord> fetchAllOnDate(Date date)
			throws DAOException {
		List<AppointmentRecord> appointments = new ArrayList<AppointmentRecord>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_APPOINTMENT_ON_DATE);
			preparedStatement.setDate(1, new java.sql.Date(date.getTime()));
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				appointments.add(getAppointmentFromResultSet(resultSet));
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return appointments;
	}
	
	@Override
	public List<AppointmentRecord> fetchPatientAppointments(int id) throws DAOException {
		List<AppointmentRecord> appointments = new ArrayList<AppointmentRecord>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_APPOINTMENT_ON_PATIENTID);
			preparedStatement.setInt(1,id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				appointments.add(getAppointmentFromResultSet(resultSet));
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return appointments;
	}
	
	private AppointmentRecord getAppointmentFromResultSet(ResultSet resultSet) throws SQLException{
		AppointmentRecord appointment = new AppointmentRecord();
		appointment.setAppointmentRecordId(resultSet.getInt(1));
		appointment.setPatientId(resultSet.getInt(2));
		appointment.setDoctorId(resultSet.getInt(3));
		appointment.setAppointmentId(resultSet.getInt(4));
		Timestamp ts = resultSet.getTimestamp(5);
		if(ts != null){
			appointment.setDate(new Date(ts.getTime()));
		}
		appointment.setPerfomedStaffId(resultSet.getInt(6));
		appointment.setDosage(resultSet.getFloat(7));
		return appointment;
	}

	@Override
	public void deleteAllAppointmentsOnId(int appointmentId)
			throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_DELETE_ALL_APPOINTMENTS_ON_ID);
			preparedStatement.setInt(1, appointmentId);
			preparedStatement.executeUpdate();
		} catch(SQLException e){
			throw new DAOException("Fail delete Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public List<AppointmentRecord> fetchOperationsOnToday(int departmentId)
			throws DAOException {
		List<AppointmentRecord> appointments = new ArrayList<AppointmentRecord>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_OPERATIONS_ON_TODAY);
			preparedStatement.setInt(1,departmentId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				appointments.add(getAppointmentFromResultSet(resultSet));
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return appointments;
	}

	@Override
	public List<AppointmentRecord> fetchAppointmentsOnToday(int departmentId)
			throws DAOException {
		List<AppointmentRecord> appointments = new ArrayList<AppointmentRecord>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_APPOINTMENTS_ON_TODAY);
			preparedStatement.setInt(1,departmentId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				appointments.add(getAppointmentFromResultSet(resultSet));
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return appointments;
	}

}
