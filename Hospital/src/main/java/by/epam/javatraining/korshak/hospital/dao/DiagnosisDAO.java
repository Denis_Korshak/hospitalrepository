package by.epam.javatraining.korshak.hospital.dao;

import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;

public interface DiagnosisDAO extends CommonDAO<Diagnosis> {

	List<Diagnosis> fetchAllDiagnoses() throws DAOException;
	
}
