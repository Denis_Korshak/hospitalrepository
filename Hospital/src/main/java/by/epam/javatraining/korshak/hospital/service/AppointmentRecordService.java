package by.epam.javatraining.korshak.hospital.service;

import java.util.Date;
import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.AppointmentRecord;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public interface AppointmentRecordService extends CommonService<AppointmentRecord> {

	List<AppointmentRecord> fetchAllOnDate(Date date) throws ServiceException;
	List<AppointmentRecord> fetchPatientAppointments(int id) throws ServiceException;
	void deleteAllAppointmentsOnId(int appointmentId) throws ServiceException;
	List<AppointmentRecord> fetchAppointmentsOnToday(Staff staff) throws ServiceException;
}
