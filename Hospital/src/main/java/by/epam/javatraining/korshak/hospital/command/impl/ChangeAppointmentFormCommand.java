package by.epam.javatraining.korshak.hospital.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Appointment;
import by.epam.javatraining.korshak.hospital.entity.role.AppointmentType;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.AppointmentService;
import by.epam.javatraining.korshak.hospital.service.impl.AppointmentServiceImpl;

public class ChangeAppointmentFormCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		AppointmentService appointmentService = new AppointmentServiceImpl();
		
		String appointmentType = request.getParameter("appointmenttype");
		
		if(appointmentType != null){
			List<Appointment> appointmentList = appointmentService
					.fetchAppointmentsOnType(AppointmentType.valueOf(appointmentType.toUpperCase()));
			request.setAttribute("appointmentList", appointmentList);
		}
		
		String page = null;
		switch(appointmentType){
		case "medication":
			page = ResourceManager.getProperty("page.medication");
			break;
		case "procedure":
			page = ResourceManager.getProperty("page.procedure");
			break;
		case "operation":
			page = ResourceManager.getProperty("page.operation");
			break;
		default:
			page = ResourceManager.getProperty("page.appointments");
			break;
		}
		
		return page;
	}

}
