package by.epam.javatraining.korshak.hospital.command.utils;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.impl.AddAppointmentRecordCommand;
import by.epam.javatraining.korshak.hospital.command.impl.AddDiagnosisCommand;
import by.epam.javatraining.korshak.hospital.command.impl.ChangeAppointmentFormCommand;
import by.epam.javatraining.korshak.hospital.command.impl.ChangeLocaleCommand;
import by.epam.javatraining.korshak.hospital.command.impl.DeleteAppointmentCommand;
import by.epam.javatraining.korshak.hospital.command.impl.DeleteDiagnosisCommand;
import by.epam.javatraining.korshak.hospital.command.impl.EmptyCommand;
import by.epam.javatraining.korshak.hospital.command.impl.FinishInspectionCommand;
import by.epam.javatraining.korshak.hospital.command.impl.LinkCommand;
import by.epam.javatraining.korshak.hospital.command.impl.LoginCommand;
import by.epam.javatraining.korshak.hospital.command.impl.LogoutCommand;
import by.epam.javatraining.korshak.hospital.command.impl.PerfomeAppointmentCommand;
import by.epam.javatraining.korshak.hospital.command.impl.RegistratePatientCommand;
import by.epam.javatraining.korshak.hospital.command.impl.SearchPatientCommand;
import by.epam.javatraining.korshak.hospital.command.impl.ShowAllPatientsCommand;
import by.epam.javatraining.korshak.hospital.command.impl.ShowAppointmentsOnToday;
import by.epam.javatraining.korshak.hospital.command.impl.ShowNewPatientsCommand;
import by.epam.javatraining.korshak.hospital.command.impl.ShowPatientAppointmentsCommand;
import by.epam.javatraining.korshak.hospital.command.impl.ShowPatientDiagnosesCommand;
import by.epam.javatraining.korshak.hospital.command.impl.ShowStaffsCommand;
import by.epam.javatraining.korshak.hospital.command.impl.ShowTakeOutCommand;
import by.epam.javatraining.korshak.hospital.command.impl.TakeOutCommand;

public class CommandUtil {

	private static final String LOGIN = "login";
	private static final String STAFF_LIST = "staff_list";
	private static final String REGISTRATE_PATIENT = "registrate_patient";
	private static final String LINK = "link";
	private static final String SHOW_ALL_PATIENTS = "patients";
	private static final String SHOW_NEW_PATIENTS = "new_patients";
	private static final String SEARCH_PATIENT = "search_patient";
	private static final String SHOW_PATIENT_DIAGNOSES = "patient_diagnoses";
	private static final String ADD_DIAGNOSIS = "add_diagnosis";
	private static final String DELETE_DIAGNOSIS = "delete_diagnosis";
	private static final String SHOW_PATIENT_APPOINTMENTS = "patient_appointments";
	private static final String CHANGE_FORM = "change_form";
	private static final String ADD_APPOINTMENT = "add_appointment";
	private static final String DELETE_APPOINTMENT = "delete_appointment";
	private static final String FINISH_INSPECTION = "finish_inspection";
	private static final String APPOINTMENTS_ON_TODAY = "appointments_on_today";
	private static final String PERFOME_APPOINTMENT = "perfome_appointment";
	private static final String TAKE_OUT_PAGE = "take_out_page";
	private static final String TAKE_OUT = "take_out";
	private static final String CHANGE_LOCALE = "change_locale";
	private static final String LOG_OUT = "log_out";
	
	public static ActionCommand createCommand(String commandName){
		ActionCommand command = null;
		if (commandName == null){
			return new EmptyCommand();
		}
		switch(commandName.toLowerCase()){
			case LOGIN:
				command = new LoginCommand();
				break;
			case STAFF_LIST:
				command = new ShowStaffsCommand();
				break;
			case REGISTRATE_PATIENT:
				command = new RegistratePatientCommand();
				break;
			case LINK:
				command = new LinkCommand();
				break;
			case SHOW_ALL_PATIENTS:
				command = new ShowAllPatientsCommand();
				break;
			case SHOW_NEW_PATIENTS:
				command = new ShowNewPatientsCommand();
				break;
			case SEARCH_PATIENT:
				command = new SearchPatientCommand();
				break;
			case SHOW_PATIENT_DIAGNOSES:
				command = new ShowPatientDiagnosesCommand();
				break;
			case ADD_DIAGNOSIS:
				command = new AddDiagnosisCommand();
				break;
			case DELETE_DIAGNOSIS:
				command = new DeleteDiagnosisCommand();
				break;
			case SHOW_PATIENT_APPOINTMENTS:
				command = new ShowPatientAppointmentsCommand();
				break;
			case CHANGE_FORM:
				command = new ChangeAppointmentFormCommand();
				break;
			case ADD_APPOINTMENT:
				command = new AddAppointmentRecordCommand();
				break;
			case DELETE_APPOINTMENT:
				command = new DeleteAppointmentCommand();
				break;
			case FINISH_INSPECTION:
				command = new FinishInspectionCommand();
				break;
			case APPOINTMENTS_ON_TODAY:
				command = new ShowAppointmentsOnToday();
				break;
			case PERFOME_APPOINTMENT:
				command = new PerfomeAppointmentCommand();
				break;
			case TAKE_OUT_PAGE:
				command = new ShowTakeOutCommand();
				break;
			case TAKE_OUT:
				command = new TakeOutCommand();
				break;
			case CHANGE_LOCALE:
				command = new ChangeLocaleCommand();
				break;
			case LOG_OUT:
				command = new LogoutCommand();
				break;
			default:
				command = new EmptyCommand();
				break;
		}
		return command;
	}
}
