package by.epam.javatraining.korshak.hospital.service;

import by.epam.javatraining.korshak.hospital.exeption.ServiceException;


public interface LoginService {
	
	public int loginUser(String login, String password) throws ServiceException;
	
}
