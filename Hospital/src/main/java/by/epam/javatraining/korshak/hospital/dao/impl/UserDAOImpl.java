package by.epam.javatraining.korshak.hospital.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import by.epam.javatraining.korshak.hospital.dao.UserDAO;
import by.epam.javatraining.korshak.hospital.entity.User;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.poolconnections.PoolConnections;

public class UserDAOImpl implements UserDAO {

	private static final String SQL_FETCH_USER_ON_LOGIN_PASSWORD = 
			"SELECT id_staff from LOGIN WHERE login = ? and password = ?";
	
	@Override
	public boolean create(User entity) throws DAOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public User fetch(int id) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(User entity) throws DAOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) throws DAOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void close(Statement st) throws DAOException {
		// TODO Auto-generated method stub

	}

	@Override
	public int fetchUserIdByLoginPassword(String login, String password)
			throws DAOException {
		int staffId = -1;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_USER_ON_LOGIN_PASSWORD);
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				staffId = resultSet.getInt(1);
			}
		} catch(SQLException e){
			throw new DAOException("Fail insert into Staff.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return staffId;
	}

}
