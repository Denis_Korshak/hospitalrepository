package by.epam.javatraining.korshak.hospital.entity;

public class Department {

	private int departmentId;
	private String name;
	private int mainDoctorId;
	
	public Department(){}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMainDoctorId() {
		return mainDoctorId;
	}

	public void setMainDoctorId(int mainDoctorId) {
		this.mainDoctorId = mainDoctorId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + departmentId;
		result = prime * result + mainDoctorId;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Department other = (Department) obj;
		if (departmentId != other.departmentId)
			return false;
		if (mainDoctorId != other.mainDoctorId)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Department [departmentId=" + departmentId + ", name=" + name
				+ ", mainDoctorId=" + mainDoctorId + "]";
	}
	
	
}
