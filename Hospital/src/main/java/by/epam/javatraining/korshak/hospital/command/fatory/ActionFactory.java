package by.epam.javatraining.korshak.hospital.command.fatory;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.CommandUtil;

public class ActionFactory {

	private static final String PARAM_NAME_COMMAND = "command";
	
	public ActionCommand defineCommand(HttpServletRequest request){
		ActionCommand command = null;
		String actionName = request.getParameter(PARAM_NAME_COMMAND);
		command = CommandUtil.createCommand(actionName);
		return command;
	}
}
