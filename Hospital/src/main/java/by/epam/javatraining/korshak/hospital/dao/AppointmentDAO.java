package by.epam.javatraining.korshak.hospital.dao;

import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.Appointment;
import by.epam.javatraining.korshak.hospital.entity.role.AppointmentType;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;

public interface AppointmentDAO extends CommonDAO<Appointment> {

	int fetchIdOnName(String name) throws DAOException;
	List<Appointment> fetchAppointmentsOnType(AppointmentType type) throws DAOException;
	List<Appointment> fetchPatientAppointments(int patientId) throws DAOException;
	
}
