package by.epam.javatraining.korshak.hospital.entity;

import java.util.Date;

public class AppointmentRecord {

	private int appointmentRecordId;
	private int patientId;
	private int doctorId;
	private int appointmentId;
	private Date date;
	private int perfomedStaffId;
	private float dosage;
	
	public AppointmentRecord(){}

	public int getAppointmentRecordId() {
		return appointmentRecordId;
	}

	public void setAppointmentRecordId(int appointmentRecordId) {
		this.appointmentRecordId = appointmentRecordId;
	}

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public int getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(int appointmentId) {
		this.appointmentId = appointmentId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getPerfomedStaffId() {
		return perfomedStaffId;
	}

	public void setPerfomedStaffId(int perfomedStaffId) {
		this.perfomedStaffId = perfomedStaffId;
	}

	public float getDosage() {
		return dosage;
	}

	public void setDosage(float dosage) {
		this.dosage = dosage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + appointmentId;
		result = prime * result + appointmentRecordId;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + doctorId;
		result = prime * result + Float.floatToIntBits(dosage);
		result = prime * result + patientId;
		result = prime * result + perfomedStaffId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppointmentRecord other = (AppointmentRecord) obj;
		if (appointmentId != other.appointmentId)
			return false;
		if (appointmentRecordId != other.appointmentRecordId)
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (doctorId != other.doctorId)
			return false;
		if (Float.floatToIntBits(dosage) != Float.floatToIntBits(other.dosage))
			return false;
		if (patientId != other.patientId)
			return false;
		if (perfomedStaffId != other.perfomedStaffId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AppointmentRecord [appointmentRecordId=" + appointmentRecordId
				+ ", patientId=" + patientId + ", doctorId=" + doctorId
				+ ", appointmentId=" + appointmentId + ", date=" + date
				+ ", perfomedStaffId=" + perfomedStaffId + ", dosage=" + dosage
				+ "]";
	}
	
	
}
