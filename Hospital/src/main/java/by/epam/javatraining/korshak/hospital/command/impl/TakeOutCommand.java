package by.epam.javatraining.korshak.hospital.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.PatientService;
import by.epam.javatraining.korshak.hospital.service.impl.PatientServiceImpl;

public class TakeOutCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		
		PatientService patientService = new PatientServiceImpl();
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null){
			return ResourceManager.getProperty("page.login");
		}
		
		Patient patient = (Patient)request.getSession().getAttribute("patient");
		if(patient == null){
			return (new ShowAllPatientsCommand()).execute(request);
		}
		
		String diagnosisId = request.getParameter("diagnosisId");
		patientService.takePatientOut(patient, Integer.parseInt(diagnosisId));
		request.getSession().removeAttribute("patient");
		
		return (new ShowAllPatientsCommand()).execute(request);
	}

}
