package by.epam.javatraining.korshak.hospital.dao;

import by.epam.javatraining.korshak.hospital.entity.Department;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;

public interface DepartmentDAO extends CommonDAO<Department> {

	Department fetchDepartmentOnName(String name) throws DAOException;

}
