package by.epam.javatraining.korshak.hospital.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.AppointmentRecordDAO;
import by.epam.javatraining.korshak.hospital.dao.impl.AppointmentRecordDAOImpl;
import by.epam.javatraining.korshak.hospital.entity.AppointmentRecord;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.entity.role.StaffRole;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.AppointmentRecordService;

public class AppointmentRecordServiceImpl implements AppointmentRecordService {

	private AppointmentRecordDAO appointmentRecorDAO;
	
	public AppointmentRecordServiceImpl(){
		appointmentRecorDAO = new AppointmentRecordDAOImpl();
	}
	
	@Override
	public boolean create(AppointmentRecord entity) throws ServiceException {
		boolean result = false;
		try {
			result = appointmentRecorDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail creating new appointment record " + entity,e);
		}
		return result;
	}

	@Override
	public void update(AppointmentRecord entity) throws ServiceException {
		try {
			appointmentRecorDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail updating appointment record " + entity,e);
		}
	}

	@Override
	public AppointmentRecord read(int id) throws ServiceException {
		AppointmentRecord record = null;
		try {
			record = appointmentRecorDAO.fetch(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading appointment record on id = " + id,e);
		}
		return record;
	}

	@Override
	public void delete(int id) throws ServiceException {
		try {
			 appointmentRecorDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail deleting appointment record on id = " + id,e);
		}
	}

	@Override
	public List<AppointmentRecord> fetchAllOnDate(Date date)
			throws ServiceException {
		List<AppointmentRecord> records = null;
		try {
			records = appointmentRecorDAO.fetchAllOnDate(date);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading appointment records on date = " + date,e);
		}
		return records;
	}

	@Override
	public List<AppointmentRecord> fetchPatientAppointments(int id)
			throws ServiceException {
		List<AppointmentRecord> records = null;
		try {
			records = appointmentRecorDAO.fetchPatientAppointments(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading appointment record on patient id = " + id,e);
		}
		return records;
	}

	@Override
	public void deleteAllAppointmentsOnId(int appointmentId)
			throws ServiceException {
		try {
			 appointmentRecorDAO.deleteAllAppointmentsOnId(appointmentId);
		} catch (DAOException e) {
			throw new ServiceException("Fail deleting appointment records on appointment id = " + appointmentId,e);
		}
	}

	@Override
	public List<AppointmentRecord> fetchAppointmentsOnToday(Staff staff)
			throws ServiceException {
		List<AppointmentRecord> list = null;
		if(staff.getRole() == StaffRole.DOCTOR){
			try {
				System.out.println("fetch");
				list = appointmentRecorDAO.fetchOperationsOnToday(staff.getDepartmentId());
				
			} catch (DAOException e) {
				throw new ServiceException("Fail fetching operations",e);
			}
		} else if(staff.getRole() == StaffRole.NURSE){
			try {
				list = appointmentRecorDAO.fetchAppointmentsOnToday(staff.getDepartmentId());
			} catch (DAOException e) {
				throw new ServiceException("Fail fetching appointments",e);
			}
		}
		return (List<AppointmentRecord>) (list == null ? Collections.emptyList() : list);
	}

}
