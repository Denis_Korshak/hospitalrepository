package by.epam.javatraining.korshak.hospital.service.impl;

import by.epam.javatraining.korshak.hospital.dao.DepartmentDAO;
import by.epam.javatraining.korshak.hospital.dao.impl.DepartmentDAOImpl;
import by.epam.javatraining.korshak.hospital.entity.Department;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.DepartmentService;

public class DepartmentServiceImpl implements DepartmentService {

	private DepartmentDAO departmentDAO;
	
	public DepartmentServiceImpl(){
		departmentDAO = new DepartmentDAOImpl();
	}
	
	@Override
	public boolean create(Department entity) throws ServiceException {
		boolean result = false;
		try {
			result = departmentDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail creating department " + entity,e);
		}
		return result;
	}

	@Override
	public void update(Department entity) throws ServiceException {
		try {
			departmentDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail updating department " + entity,e);
		}
	}

	@Override
	public Department read(int id) throws ServiceException {
		Department department = null;
		try {
			department = departmentDAO.fetch(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading department on id " + id,e);
		}
		return department;
	}

	@Override
	public void delete(int id) throws ServiceException {
		try {
			departmentDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail deleting department on id " + id,e);
		}
	}

	@Override
	public Department fetchDepartmentOnName(String name)
			throws ServiceException {
		Department department = null;
		try {
			department = departmentDAO.fetchDepartmentOnName(name);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading department on name = " + name,e);
		}
		return department;
	}

}
