package by.epam.javatraining.korshak.hospital.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.ArchiveRecordDAO;
import by.epam.javatraining.korshak.hospital.entity.ArchiveRecord;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.poolconnections.PoolConnections;

public class ArchiveRecordDAOImpl implements ArchiveRecordDAO {

	private static final String SQL_CREATE_ARCHIVE_RECORD = 
			"INSERT INTO ARCHIVE(SURNAME, NAME, PATRONYMIC, "
			+"EXIT_DATE, ID_DIAGNOSIS) "
			+ "VALUES(?,?,?,?,?)";
	
	private static final String SQL_FETCH_ARCHIVE_RECORD_ON_ID = 
			"SELECT ID_RECORD, SURNAME, NAME, "
			+"PATRONYMIC, EXIT_DATE, ID_DIAGNOSIS "
			+ "from ARCHIVE where ID_RECORD = ?";
	
	private static final String SQL_UPDATE_ARCHIVE	 = 
			"UPDATE ARCHIVE SET "
			+ "SURNAME = ?, "
			+ "NAME = ?, "
			+ "PATRONYMIC = ?, "
			+ "EXIT_DATE = ?, "
			+ "ID_DIAGNOSIS = ? "
			+ "WHERE ID_RECORD = ?";
	
	private static final String SQL_DELETE_ARCHIVE = 
			"DELETE FROM ARCHIVE WHERE ID_RECORD = ?";
	
	private static final String SQL_FETCH_ARCHIVE_RECORD_ON_NAME = 
			"SELECT ID_RECORD, SURNAME, NAME, "
			+"PATRONYMIC, EXIT_DATE, ID_DIAGNOSIS "
			+ "from ARCHIVE where SURNAME = ? and NAME = ? and PATRONYMIC = ?";
	
	@Override
	public boolean create(ArchiveRecord entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_CREATE_ARCHIVE_RECORD,Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, entity.getSurname());
			preparedStatement.setString(2, entity.getName());		
			preparedStatement.setString(3, entity.getPatronymic());	
			preparedStatement.setDate(4, new java.sql.Date(entity.getExitDate().getTime()));
			preparedStatement.setInt(5, entity.getDiagnosisId());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			return resultSet.next();
		} catch(SQLException e){
			throw new DAOException("Fail insert into Archive.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public ArchiveRecord fetch(int id) throws DAOException {
		ArchiveRecord archive = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_ARCHIVE_RECORD_ON_ID);
			preparedStatement.setInt(1,id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				archive = getArchiveFromResultSet(resultSet);
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Archive.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return archive;
	}

	@Override
	public void update(ArchiveRecord entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_UPDATE_ARCHIVE);
			preparedStatement.setString(1, entity.getSurname());
			preparedStatement.setString(2, entity.getName());		
			preparedStatement.setString(3, entity.getPatronymic());	
			preparedStatement.setDate(4, new java.sql.Date(entity.getExitDate().getTime()));
			preparedStatement.setInt(5, entity.getDiagnosisId());
			preparedStatement.setInt(6, entity.getArchiveRecordId());
			preparedStatement.executeUpdate();
		} catch(SQLException e){
			throw new DAOException("Fail update Archive.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void delete(int id) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_DELETE_ARCHIVE);
			preparedStatement.setInt(1, id);
			preparedStatement.executeQuery();
		} catch(SQLException e){
			throw new DAOException("Fail delete Archive.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void close(Statement st) throws DAOException {
		try{
			if(st != null){
				st.close();
			}
		} catch(SQLException e){
			throw new DAOException("Can't close statement.",e);
		}
	}

	@Override
	public List<ArchiveRecord> fetchOnName(String surname, String name,
			String patronymic) throws DAOException {
		List<ArchiveRecord> archive = new ArrayList<ArchiveRecord>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_ARCHIVE_RECORD_ON_NAME);
			preparedStatement.setString(1,surname);
			preparedStatement.setString(2,name);
			preparedStatement.setString(3,patronymic);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				archive.add(getArchiveFromResultSet(resultSet));
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Archive.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return archive;
	}
	
	private ArchiveRecord getArchiveFromResultSet(ResultSet resultSet) throws SQLException{
		ArchiveRecord archive = new ArchiveRecord();
		archive.setArchiveRecordId(resultSet.getInt(1));
		archive.setSurname(resultSet.getString(2));
		archive.setName(resultSet.getString(3));
		archive.setPatronymic(resultSet.getString(4));
		archive.setExitDate(resultSet.getDate(5));
		archive.setDiagnosisId(resultSet.getInt(6));
		return archive;
	}

}
