package by.epam.javatraining.korshak.hospital.exeption;

public class DAOException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7099565441269458708L;

	public DAOException(){
		super();
	}
	
	public DAOException(String message){
		super(message);
	}
	
	public DAOException(String message, Throwable error){
		super(message, error);
	}
	
	public DAOException(Throwable error){
		super(error);
	}
}
