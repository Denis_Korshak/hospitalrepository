package by.epam.javatraining.korshak.hospital.command.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.PatientService;
import by.epam.javatraining.korshak.hospital.service.impl.PatientServiceImpl;
import static by.epam.javatraining.korshak.hospital.paramname.ParamName.*;

public class RegistratePatientCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null || !user.isRegistry()){
			return ResourceManager.getProperty("page.index");
		}
		
		Patient patient = new Patient();
		patient.setSurname(request.getParameter(SURNAME));
		patient.setName(request.getParameter(NAME));
		patient.setPatronymic(request.getParameter(PATRONYMIC));
		DateFormat format = new SimpleDateFormat("d.M.y", Locale.ENGLISH);
		try {
			patient.setDateOfBirth(format.parse(request.getParameter(DATE_OF_BIRTH)));
		} catch (ParseException e) {
			throw new ServiceException("Illegal date format",e);
		}

		patient.setDepartmentId(Integer.parseInt(request.getParameter(DEPARTMENT)));
		patient.setWard(Integer.parseInt(request.getParameter(WARD)));
		
		PatientService patientService = new PatientServiceImpl();
		boolean result = patientService.create(patient);
		
		if(result){
			request.setAttribute("message", "������� ��������");
		} else {
			request.setAttribute("message", "������ ���������� ��������");
		}
		
		return ResourceManager.getProperty("page.registrate");
	}

}
