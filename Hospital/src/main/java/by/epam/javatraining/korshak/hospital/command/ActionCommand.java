package by.epam.javatraining.korshak.hospital.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public interface ActionCommand {
	String execute(HttpServletRequest request) throws ServiceException;
}
