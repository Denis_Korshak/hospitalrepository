package by.epam.javatraining.korshak.hospital.customtag;

import java.io.IOException;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class ColumnTag extends TagSupport {

	private String head;
	private String field;
	
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	
	protected TableTag getTableTag()
    {
        return (TableTag) findAncestorWithClass(this, TableTag.class);
    }
	
	public int doStartTag() throws JspTagException{
		TableTag tableTag = getTableTag();
		if(tableTag.isFirstIteration()){
			addHeader();
		} else {
			addCell(tableTag);
		}
		return SKIP_BODY;
	}
	
	private void addHeader() throws JspTagException{
		JspWriter out = pageContext.getOut();
		try {
			out.write("<td>"+head+"</td>");
		} catch (IOException e) {
			throw new JspTagException(e.getMessage());
		}
	}
	
	private void addCell(TableTag tableTag) throws JspTagException {
		JspWriter out = pageContext.getOut();
		try {
			out.write("<td>"+ TagAttributeUtil.parseFieldValue(field, tableTag.getElement()) + "</td>");
		} catch (IOException e) {
			throw new JspTagException(e.getMessage());
		}
	}
}
