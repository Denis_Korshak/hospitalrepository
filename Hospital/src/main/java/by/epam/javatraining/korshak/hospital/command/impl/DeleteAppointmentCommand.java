package by.epam.javatraining.korshak.hospital.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.AppointmentRecordService;
import by.epam.javatraining.korshak.hospital.service.AppointmentService;
import by.epam.javatraining.korshak.hospital.service.impl.AppointmentRecordServiceImpl;
import by.epam.javatraining.korshak.hospital.service.impl.AppointmentServiceImpl;

public class DeleteAppointmentCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		AppointmentRecordService appointmentRecordService = new AppointmentRecordServiceImpl();
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null){
			return ResourceManager.getProperty("page.index");
		}
		
		Patient patient = (Patient)request.getSession().getAttribute("patient");
		if(patient == null){
			return ResourceManager.getProperty("page.patients");
		}
		
		String appointmentToDelete = request.getParameter("appointmentToDelete");
		int appointmentId = Integer.parseInt(appointmentToDelete);
		
		if(user.isDoctor()){
			appointmentRecordService.deleteAllAppointmentsOnId(appointmentId);
			return (new ShowPatientAppointmentsCommand()).execute(request);
		} else {
			return ResourceManager.getProperty("page.index");
		}
		
	}

}
