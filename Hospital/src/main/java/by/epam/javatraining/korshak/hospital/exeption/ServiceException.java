package by.epam.javatraining.korshak.hospital.exeption;

public class ServiceException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3104515449125046471L;

	public ServiceException(){
		super();
	}
	
	public ServiceException(String message){
		super(message);
	}
	
	public ServiceException(String message, Throwable error){
		super(message, error);
	}
	
	public ServiceException(Throwable error){
		super(error);
	}
}
