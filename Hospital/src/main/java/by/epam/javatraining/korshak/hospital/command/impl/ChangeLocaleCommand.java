package by.epam.javatraining.korshak.hospital.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public class ChangeLocaleCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String localeSelected = request.getParameter("localeSelected");
		request.getSession().setAttribute("locale", localeSelected);
		return ResourceManager.getProperty("page.index");
	}

}
