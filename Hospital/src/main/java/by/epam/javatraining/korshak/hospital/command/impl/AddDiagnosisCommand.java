package by.epam.javatraining.korshak.hospital.command.impl;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.DiagnosisService;
import by.epam.javatraining.korshak.hospital.service.PatientService;
import by.epam.javatraining.korshak.hospital.service.impl.DiagnosisServiceImpl;
import by.epam.javatraining.korshak.hospital.service.impl.PatientServiceImpl;

public class AddDiagnosisCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		PatientService patientService = new PatientServiceImpl();
		
		String diagnosisIdToAdd = request.getParameter("diagnosisToAdd");
		
		Patient patient = (Patient)request.getSession().getAttribute("patient");
		if(patient == null){
			return ResourceManager.getProperty("page.diagnoses");
		}
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null){
			return ResourceManager.getProperty("page.index");
		}
		
		if(user.isDoctor()){
			patientService.addDiagnosis(patient.getPatientId(), Integer.parseInt(diagnosisIdToAdd));
			return (new ShowPatientDiagnosesCommand()).execute(request);
		} else {
			return ResourceManager.getProperty("page.index");
		}
		
		
	}

}
