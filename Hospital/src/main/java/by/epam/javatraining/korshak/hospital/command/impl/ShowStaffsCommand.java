package by.epam.javatraining.korshak.hospital.command.impl;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.StaffService;
import by.epam.javatraining.korshak.hospital.service.impl.StaffServiceImpl;

public class ShowStaffsCommand implements ActionCommand{

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		StaffService staffService = new StaffServiceImpl();
		List<Staff> staffs = staffService.fetchStaff();
		staffs = (List<Staff>) (staffs == null ? Collections.emptyList() : staffs);
		request.setAttribute("staffs", staffs);
		return "/jsp/home.jsp";
	}

}
