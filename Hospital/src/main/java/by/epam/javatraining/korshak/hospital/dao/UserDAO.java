package by.epam.javatraining.korshak.hospital.dao;

import by.epam.javatraining.korshak.hospital.entity.User;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;

public interface UserDAO extends CommonDAO<User> {
	
	int fetchUserIdByLoginPassword(String login, String password) throws DAOException;
	
}
