package by.epam.javatraining.korshak.hospital.service.impl;

import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.StaffDAO;
import by.epam.javatraining.korshak.hospital.dao.impl.StaffDAOImpl;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.StaffService;

public class StaffServiceImpl implements StaffService{

	private StaffDAO staffDAO;
	
	public StaffServiceImpl(){
		staffDAO = new StaffDAOImpl();
	}
	
	@Override
	public List<Staff> fetchStaff() throws ServiceException {
		try {
			return staffDAO.fetchStaff();
		} catch (DAOException e) {
			throw new ServiceException("Fail fetching staff", e);
		}
	}

	@Override
	public boolean create(Staff entity) throws ServiceException {
		boolean result = false;
		try {
			result = staffDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail creating new staff " + entity, e);
		}
		return result;
	}

	@Override
	public void update(Staff entity) throws ServiceException {
		try {
			staffDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail updating staff " + entity, e);
		}
	}

	@Override
	public Staff read(int id) throws ServiceException {
		Staff staff = null;
		try {
			staff = staffDAO.fetch(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail reaing staff on id = " + id, e);
		}
		return staff;
	}

	@Override
	public void delete(int id) throws ServiceException {
		try {
			staffDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail deleting staff on id = " + id, e);
		}
	}

}
