package by.epam.javatraining.korshak.hospital.service.impl;

import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.ArchiveRecordDAO;
import by.epam.javatraining.korshak.hospital.dao.impl.ArchiveRecordDAOImpl;
import by.epam.javatraining.korshak.hospital.entity.ArchiveRecord;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.ArchiveRecordService;

public class ArchiveRecordServiceImpl implements ArchiveRecordService {

	private ArchiveRecordDAO archiveDAO;
	
	public ArchiveRecordServiceImpl() {
		archiveDAO = new ArchiveRecordDAOImpl();
	}

	@Override
	public boolean create(ArchiveRecord entity) throws ServiceException {
		boolean result = false;
		try {
			result = archiveDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail creating archive record " + entity,e);
		}
		return result;
	}

	@Override
	public void update(ArchiveRecord entity) throws ServiceException {
		try {
			archiveDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail updating archive record " + entity,e);
		}
	}

	@Override
	public ArchiveRecord read(int id) throws ServiceException {
		ArchiveRecord record = null;
		try {
			record = archiveDAO.fetch(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading archive record on id = " + id,e);
		}
		return record;
	}

	@Override
	public void delete(int id) throws ServiceException {
		try {
			archiveDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail deleting archive record on id = " + id,e);
		}
	}

	@Override
	public List<ArchiveRecord> fetchOnName(String surname, String name,
			String patronymic) throws ServiceException {
		List<ArchiveRecord> records = null;
		try {
			records = archiveDAO.fetchOnName(surname, name, patronymic);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading archive record on name",e);
		}
		return records;
	}
	

}
