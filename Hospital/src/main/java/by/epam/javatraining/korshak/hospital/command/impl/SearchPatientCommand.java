package by.epam.javatraining.korshak.hospital.command.impl;

import static by.epam.javatraining.korshak.hospital.paramname.ParamName.*;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.PatientService;
import by.epam.javatraining.korshak.hospital.service.impl.PatientServiceImpl;

public class SearchPatientCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		PatientService service = new PatientServiceImpl();
		
		String surname = request.getParameter(SURNAME);
		
		if(surname != null){
			List<Patient> patients = service.searchOnSurname(surname);
			patients = (List<Patient>) (patients == null ? Collections.emptyList() : patients);
			request.setAttribute("patients", patients);
		} else {
			request.setAttribute("message", "������� ��������");
		}
		
		return ResourceManager.getProperty("page.patients");
	}

}
