package by.epam.javatraining.korshak.hospital.service;

import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.ArchiveRecord;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public interface ArchiveRecordService extends CommonService<ArchiveRecord> {

	List<ArchiveRecord> fetchOnName(String surname, String name, String patronymic) throws ServiceException;
}
