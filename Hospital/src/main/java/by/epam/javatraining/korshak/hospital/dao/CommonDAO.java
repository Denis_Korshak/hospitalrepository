package by.epam.javatraining.korshak.hospital.dao;

import java.sql.Statement;

import by.epam.javatraining.korshak.hospital.exeption.DAOException;

public interface CommonDAO<T> {

	boolean create(T entity) throws DAOException;

	T fetch(int id) throws DAOException;

	void update(T entity) throws DAOException;

	void delete(int id) throws DAOException;

	void close(Statement st) throws DAOException;
}
