package by.epam.javatraining.korshak.hospital.command.impl;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Appointment;
import by.epam.javatraining.korshak.hospital.entity.AppointmentRecord;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.entity.role.AppointmentType;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.AppointmentRecordService;
import by.epam.javatraining.korshak.hospital.service.AppointmentService;
import by.epam.javatraining.korshak.hospital.service.PatientService;
import by.epam.javatraining.korshak.hospital.service.impl.AppointmentRecordServiceImpl;
import by.epam.javatraining.korshak.hospital.service.impl.AppointmentServiceImpl;
import by.epam.javatraining.korshak.hospital.service.impl.PatientServiceImpl;

public class ShowPatientAppointmentsCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		PatientService patientService = new PatientServiceImpl();
		AppointmentRecordService appointmentService = new AppointmentRecordServiceImpl();
		AppointmentService appointmentListService = new AppointmentServiceImpl();
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null || !user.isDoctor()){
			return ResourceManager.getProperty("page.index");
		}
		
		Patient patient = (Patient)request.getSession().getAttribute("patient");
		String selectedId = request.getParameter("selectedId");
		if(selectedId != null){
			patient = patientService.read(Integer.parseInt(selectedId));
			request.getSession().setAttribute("patient", patient);
		}
		if(null == patient){
			return ResourceManager.getProperty("page.patients");
		}
		
		List<AppointmentRecord> appointmentRecords = appointmentService
				.fetchPatientAppointments(patient.getPatientId());
		appointmentRecords = (List<AppointmentRecord>) (appointmentRecords == null ? Collections.emptyList() : appointmentRecords);
		request.setAttribute("appointmentRecords", appointmentRecords);
			
		String appointmentType = "medication";
		List<Appointment> appointmentList = appointmentListService
				.fetchAppointmentsOnType(AppointmentType.valueOf(appointmentType.toUpperCase()));
		request.setAttribute("appointmentList", appointmentList);
		
		List<Appointment> appointmentsToDelete = appointmentListService
				.fetchPatientAppointments(patient.getPatientId());
		request.setAttribute("appointmentsToDelete", appointmentsToDelete);
		
		return ResourceManager.getProperty("page.appointments");
	}

}
