package by.epam.javatraining.korshak.hospital.poolconnections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import by.epam.javatraining.korshak.hospital.exeption.DAOException;

public class PoolConnections {
	private static int poolSize;
	private static final int MAX_WAIT_MILLIS = 10000;
	private Semaphore semaphore;
	private Queue<Connection> connectionList = new LinkedList<Connection>();
	private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
	private static AtomicBoolean closeConnection = new AtomicBoolean(false);
	private static ReentrantLock lockInstance = new ReentrantLock();
	private static ReentrantLock lockConnection = new ReentrantLock();
	private static PoolConnections instance ;
	private static final Logger LOG = Logger.getLogger(PoolConnections.class);
	
	private PoolConnections() throws DAOException {
		try{
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			
		} catch(SQLException e){
			throw new DAOException("Fail registration driver.",e);
		}
		ResourceBundle resource = ResourceBundle.getBundle("resources.database");
		poolSize = Integer.valueOf(resource.getString("db.pool.size"));
		semaphore = new Semaphore(poolSize);
		for (int i = 0; i < poolSize; i++) {
			String url = resource.getString("db.url");
			String user = resource.getString("db.user");
			String pass = resource.getString("db.password");
			Connection connection = null;
			try {
				connection = DriverManager.getConnection(url,user,pass);
				
				connectionList.add(connection);
			} catch (SQLException e) {
				throw new DAOException("Fail getting a connection.",e);
			}
		}
	}
	
	public static PoolConnections getInstance() throws DAOException{
		if(!closeConnection.get()){
			if(!instanceCreated.get()){
				lockInstance.lock();
				try{
					if(!instanceCreated.get()){
						instance = new PoolConnections();
						instanceCreated.set(true);
					}
				} catch(DAOException e){
					LOG.error(e);
				} finally{
					lockInstance.unlock();
				}
			}
			return instance;
		} else {
			throw new DAOException();
		}
	}
	
	public Connection getConnection() throws DAOException{
		try {
			lockConnection.lock();
			if(semaphore.tryAcquire(MAX_WAIT_MILLIS, TimeUnit.MILLISECONDS)){
				Connection connection = connectionList.poll();
				return connection;
			}
		} catch (InterruptedException e) {
			throw new DAOException("Fail getting a connection.",e);
		} finally{
			lockConnection.unlock();
		}
		throw new DAOException("timeout");
	}
	
	public void returnResource(Connection connection){
		this.connectionList.add(connection);
		semaphore.release();
	}
	
	public void closeConnections() {
		closeConnection.set(true);
		LOG.debug("destroy connection");
		try {
			TimeUnit.MILLISECONDS.sleep(MAX_WAIT_MILLIS);
		} catch (InterruptedException e) {
			LOG.error(e);
		}
		for (Connection connection : connectionList) {
			try {
				connection.close();
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
	}
}