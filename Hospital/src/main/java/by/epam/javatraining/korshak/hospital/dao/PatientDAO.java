package by.epam.javatraining.korshak.hospital.dao;

import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;

public interface PatientDAO extends CommonDAO<Patient> {
	
	List<Patient> fetchPatientsOnDepartment(int id) throws DAOException;
	List<Patient> fetchAllPatients() throws DAOException;
	List<Patient> fetchNewPatients(int id) throws DAOException;
	List<Patient> searchOnSurname(String surname) throws DAOException;
	void addDiagnosis(int patientId, int diagnosisId) throws DAOException;
	List<Diagnosis> fetchDiagnosis(int patientId) throws DAOException;
	void deleteDiagnosis(int patientId, int diagnosisId) throws DAOException;
}
