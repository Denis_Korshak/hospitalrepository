package by.epam.javatraining.korshak.hospital.entity.role;

public enum PatientRole {

	NEW{
		{
			this.patientStatus = "new";
		}
	},
	TREATED{
		{
			this.patientStatus = "treated";
		}
	};
	
	String patientStatus;
	
	public String getPatientRole(){
		return patientStatus;
	}
}
