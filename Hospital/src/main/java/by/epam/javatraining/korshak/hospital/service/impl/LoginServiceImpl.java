package by.epam.javatraining.korshak.hospital.service.impl;

import by.epam.javatraining.korshak.hospital.dao.UserDAO;
import by.epam.javatraining.korshak.hospital.dao.impl.UserDAOImpl;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.LoginService;

public class LoginServiceImpl implements LoginService{
	
	public int loginUser(String login, String password) throws ServiceException{
		UserDAO userDAO = new UserDAOImpl();
		try {
			return userDAO.fetchUserIdByLoginPassword(login, password);
		} catch (DAOException e) {
			throw new ServiceException("Some problem", e);
		}
		
		
	}
}
