package by.epam.javatraining.korshak.hospital.command.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.AppointmentRecord;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.AppointmentRecordService;
import by.epam.javatraining.korshak.hospital.service.impl.AppointmentRecordServiceImpl;

public class AddAppointmentRecordCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		AppointmentRecordService appointmentRecordService = new AppointmentRecordServiceImpl();
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null){
			return ResourceManager.getProperty("page.index");
		}
		
		Patient patient = (Patient)request.getSession().getAttribute("patient");
		if(patient == null){
			return ResourceManager.getProperty("page.appointments");
		}
		AppointmentRecord appointmentRecord = new AppointmentRecord();
		appointmentRecord.setDoctorId(user.getStaffId());
		appointmentRecord.setPatientId(patient.getPatientId());
		String appointmentId = request.getParameter("appointmentId");
		if(appointmentId != null){
			appointmentRecord.setAppointmentId(Integer.parseInt(appointmentId));
		}
		String dosage = request.getParameter("dosage");
		if(dosage != null){
			appointmentRecord.setDosage(Float.parseFloat(dosage));
		}
		
		DateFormat format = new SimpleDateFormat("d.M.y hh:mm",Locale.ENGLISH);
		DateFormat format2 = new SimpleDateFormat("d.M.y", Locale.ENGLISH);
		
		String dateTo = request.getParameter("dateTo");
		String[] time = request.getParameterValues("time[]");
		
		Calendar today = Calendar.getInstance();
		
		Date lastDate = new Date();
		try {
			lastDate = format2.parse(dateTo);
		} catch (ParseException e) {
			throw new ServiceException("Illegal date format", e);
		}
		while(today.getTime().before(lastDate)){
			today.add(Calendar.DATE, 1);
			for(int i = 0; i < time.length; i++){
				try {
					appointmentRecord.setDate(format.parse(format2.format(today.getTime()) + " " + time[i]));
				} catch (ParseException e) {
					throw new ServiceException("Illegal date format",e);
				}
				appointmentRecordService.create(appointmentRecord);
			}
		}	
		
		return (new ShowPatientAppointmentsCommand()).execute(request);
	}

}
