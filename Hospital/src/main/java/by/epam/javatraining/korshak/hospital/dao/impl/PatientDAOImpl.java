package by.epam.javatraining.korshak.hospital.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.PatientDAO;
import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.role.PatientRole;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.poolconnections.PoolConnections;

public class PatientDAOImpl implements PatientDAO {

	private static final String SQL_CREATE_PATIENT = 
			"INSERT INTO PATIENTS(SURNAME,NAME,PATRONYMIC,ARRIVAL_DATETIME,"
			+ "DATE_OF_BIRTH,STATUS,ID_DEPARTMENT,WARD,ID_DOCTOR) "
			+ "VALUES(?,?,?,?,?,?,?,?,?)";
	
	private static final String SQL_FETCH_PATIENT_ON_ID = 
			"SELECT ID_PATIENT,SURNAME,NAME,PATRONYMIC,ARRIVAL_DATETIME,"
			+ "DATE_OF_BIRTH,STATUS,ID_DEPARTMENT,WARD,ID_DOCTOR "
			+ "FROM PATIENTS WHERE ID_PATIENT = ?";
	
	private static final String SQL_UPDATE_PATIENT = 
			"UPDATE PATIENTS SET "
			+ "SURNAME = ?, "
			+ "NAME = ?, "
			+ "PATRONYMIC = ?, "
			+ "ARRIVAL_DATETIME = ?, "
			+ "DATE_OF_BIRTH = ?, "
			+ "STATUS = ?, "
			+ "ID_DEPARTMENT = ?, "
			+ "WARD = ?, "
			+ "ID_DOCTOR = ? WHERE ID_PATIENT = ?";
	
	private static final String SQL_DELETE_PATIENT = 
			"DELETE FROM PATIENTS WHERE ID_PATIENT = ?";
	
	private static final String SQL_FETCH_ALL_PATIENTS = 
			"SELECT ID_PATIENT,SURNAME,NAME,PATRONYMIC,ARRIVAL_DATETIME,"
					+ "DATE_OF_BIRTH,STATUS,ID_DEPARTMENT,WARD,ID_DOCTOR "
					+ "FROM PATIENTS";
	
	private static final String SQL_FETCH_PATIENTS_ON_DEPARTMENT = 
			"SELECT ID_PATIENT,SURNAME,NAME,PATRONYMIC,ARRIVAL_DATETIME,"
			+ "DATE_OF_BIRTH,STATUS,ID_DEPARTMENT,WARD,ID_DOCTOR "
			+ "FROM PATIENTS WHERE ID_DEPARTMENT = ?";
	
	private static final String SQL_FETCH_NEW_PATIENTS = 
			"SELECT ID_PATIENT,SURNAME,NAME,PATRONYMIC,ARRIVAL_DATETIME,"
			+ "DATE_OF_BIRTH,STATUS,ID_DEPARTMENT,WARD,ID_DOCTOR "
			+ "FROM PATIENTS WHERE STATUS = 'new' and ID_DEPARTMENT = ?";
	
	private static final String SQL_FETCH_PATIENT_ON_SURNAME = 
			"SELECT ID_PATIENT,SURNAME,NAME,PATRONYMIC,ARRIVAL_DATETIME,"
			+ "DATE_OF_BIRTH,STATUS,ID_DEPARTMENT,WARD,ID_DOCTOR "
			+ "FROM PATIENTS WHERE SURNAME like ?";
	
	private static final String SQL_ADD_DIAGNOSIS_TO_PATIENT = 
			"INSERT INTO patients_diagnoses(ID_PATIENT,ID_DIAGNOSIS) "
			+ "VALUES(?,?)";
	
	private static final String SQL_FETCH_PATIENT_DIAGNOSES = 
			"SELECT d.id_diagnosis, d.name from diagnoses d, patients_diagnoses pd "
			+ "where pd.id_patient = ? and pd.id_diagnosis = d.id_diagnosis";
	
	private static final String SQL_DELETE_PATIENT_DIAGNOSIS = 
			"DELETE FROM patients_diagnoses WHERE ID_PATIENT = ? and id_diagnosis = ?";
	
	@Override
	public boolean create(Patient entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_CREATE_PATIENT,Statement.RETURN_GENERATED_KEYS);
			if(entity.getSurname()!=null){
				preparedStatement.setString(1, entity.getSurname());
			} else{
				preparedStatement.setNull(1, java.sql.Types.VARCHAR);
			}
			if(entity.getName()!=null){
				preparedStatement.setString(2, entity.getName());
			} else {
				preparedStatement.setNull(2, java.sql.Types.VARCHAR);
			}
			if(entity.getPatronymic()!=null){
				preparedStatement.setString(3, entity.getPatronymic());
			} else {
				preparedStatement.setNull(3, java.sql.Types.VARCHAR);
			}
			if(entity.getArrivalDate()!=null){
				preparedStatement.setDate(4, new Date(entity.getArrivalDate().getTime()));
			} else {
				preparedStatement.setDate(4, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
			}
			if(entity.getDateOfBirth()!=null){
				preparedStatement.setDate(5, new Date(entity.getDateOfBirth().getTime()));
			} else {
				preparedStatement.setNull(5, java.sql.Types.DATE);
			}
			preparedStatement.setString(6, entity.getStatus().getPatientRole());
			if(entity.getDepartmentId()>0){
				preparedStatement.setInt(7, entity.getDepartmentId());
			} else {
				preparedStatement.setNull(7,java.sql.Types.INTEGER);
			}
			if(entity.getWard()>0){
				preparedStatement.setInt(8, entity.getWard());
			} else {
				preparedStatement.setNull(8,java.sql.Types.INTEGER);
			}
			if(entity.getDoctorId()>0){
				preparedStatement.setInt(9, entity.getDoctorId());
			} else {
				preparedStatement.setNull(9,java.sql.Types.INTEGER);
			}
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			return resultSet.next();
		} catch(SQLException e){
			throw new DAOException("Fail insert into Patients.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public Patient fetch(int id) throws DAOException {
		Patient patient = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_PATIENT_ON_ID);
			preparedStatement.setInt(1,id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				patient = getPatientFromResultSet(resultSet);
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Patients.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return patient;
	}

	@Override
	public void update(Patient entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_UPDATE_PATIENT);
			if(entity.getSurname()!=null){
				preparedStatement.setString(1, entity.getSurname());
			} else{
				preparedStatement.setNull(1, java.sql.Types.VARCHAR);
			}
			if(entity.getName()!=null){
				preparedStatement.setString(2, entity.getName());
			} else {
				preparedStatement.setNull(2, java.sql.Types.VARCHAR);
			}
			if(entity.getPatronymic()!=null){
				preparedStatement.setString(3, entity.getPatronymic());
			} else {
				preparedStatement.setNull(3, java.sql.Types.VARCHAR);
			}
			if(entity.getArrivalDate()!=null){
				preparedStatement.setDate(4, new Date(entity.getArrivalDate().getTime()));
			} else {
				preparedStatement.setDate(4, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
			}
			if(entity.getDateOfBirth()!=null){
				preparedStatement.setDate(5, new Date(entity.getDateOfBirth().getTime()));
			} else {
				preparedStatement.setNull(5, java.sql.Types.DATE);
			}
			preparedStatement.setString(6, entity.getStatus().getPatientRole());
			if(entity.getDepartmentId()>0){
				preparedStatement.setInt(7, entity.getDepartmentId());
			} else {
				preparedStatement.setNull(7,java.sql.Types.INTEGER);
			}
			if(entity.getWard()>0){
				preparedStatement.setInt(8, entity.getWard());
			} else {
				preparedStatement.setNull(8,java.sql.Types.INTEGER);
			}
			if(entity.getDoctorId()>0){
				preparedStatement.setInt(9, entity.getDoctorId());
			} else {
				preparedStatement.setNull(9,java.sql.Types.INTEGER);
			} 
			if(entity.getPatientId()>0){
				preparedStatement.setInt(10, entity.getPatientId());
			} else {
				preparedStatement.setNull(10, java.sql.Types.INTEGER);
			}
			preparedStatement.executeUpdate();
		} catch(SQLException e){
			throw new DAOException("Fail update Patient.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void delete(int id) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_DELETE_PATIENT);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
		} catch(SQLException e){
			throw new DAOException("Fail delete Patient.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void close(Statement st) throws DAOException {
		try{
			if(st != null){
				st.close();
			}
		} catch(SQLException e){
			throw new DAOException("Can't close statement.",e);
		}
	}

	@Override
	public List<Patient> fetchAllPatients() throws DAOException {
		List<Patient> result = new ArrayList<Patient>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_ALL_PATIENTS);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Patient patient = getPatientFromResultSet(resultSet);
				result.add(patient);
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetching Patient.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
				
		return result;
	}
	
	@Override
	public List<Patient> fetchPatientsOnDepartment(int id) throws DAOException {
		List<Patient> result = new ArrayList<Patient>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_PATIENTS_ON_DEPARTMENT);
			preparedStatement.setInt(1,id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Patient patient = getPatientFromResultSet(resultSet);
				result.add(patient);
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Patients.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return result;
	}
	
	private Patient getPatientFromResultSet(ResultSet resultSet) throws SQLException{
		Patient patient = new Patient();
		patient.setPatientId(resultSet.getInt(1));
		patient.setSurname(resultSet.getString(2));
		patient.setName(resultSet.getString(3));
		patient.setPatronymic(resultSet.getString(4));
		patient.setArrivalDate(resultSet.getDate(5));
		patient.setDateOfBirth(resultSet.getDate(6));
		patient.setStatus(PatientRole.valueOf(resultSet.getString(7).toUpperCase()));
		patient.setDepartmentId(resultSet.getInt(8));
		patient.setWard(resultSet.getInt(9));
		patient.setDoctorId(resultSet.getInt(10));
		return patient;
	}

	@Override
	public List<Patient> fetchNewPatients(int id) throws DAOException {
		List<Patient> result = new ArrayList<Patient>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_NEW_PATIENTS);
			preparedStatement.setInt(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Patient patient = getPatientFromResultSet(resultSet);
				result.add(patient);
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Patients.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return result;
	}

	@Override
	public List<Patient> searchOnSurname(String surname) throws DAOException {
		List<Patient> result = new ArrayList<Patient>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_PATIENT_ON_SURNAME);
			preparedStatement.setString(1,surname);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Patient patient = getPatientFromResultSet(resultSet);
				result.add(patient);
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Patients.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return result;
	}

	@Override
	public void addDiagnosis(int patientId, int diagnosisId)
			throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_ADD_DIAGNOSIS_TO_PATIENT);
			preparedStatement.setInt(1, patientId);
			preparedStatement.setInt(2, diagnosisId);
			preparedStatement.executeUpdate();
		} catch(SQLException e){
			throw new DAOException("Fail adding diagnosis to patient.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}

	}

	@Override
	public List<Diagnosis> fetchDiagnosis(int patientId) throws DAOException {
		List<Diagnosis> result = new ArrayList<Diagnosis>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_PATIENT_DIAGNOSES);
			preparedStatement.setInt(1, patientId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Diagnosis diagnosis = new Diagnosis();
				diagnosis.setDiagnosisId(resultSet.getInt(1));
				diagnosis.setName(resultSet.getString(2));
				result.add(diagnosis);
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetching patient diagnoses.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
				
		return result;
	}

	@Override
	public void deleteDiagnosis(int patientId, int diagnosisId)
			throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_DELETE_PATIENT_DIAGNOSIS);
			preparedStatement.setInt(1, patientId);
			preparedStatement.setInt(2, diagnosisId);
			preparedStatement.executeUpdate();
		} catch(SQLException e){
			throw new DAOException("Fail delete patient diagnosis.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	

}
