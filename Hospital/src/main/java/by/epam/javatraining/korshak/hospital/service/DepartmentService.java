package by.epam.javatraining.korshak.hospital.service;

import by.epam.javatraining.korshak.hospital.entity.Department;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public interface DepartmentService extends CommonService<Department> {

	Department fetchDepartmentOnName(String name) throws ServiceException;
}
