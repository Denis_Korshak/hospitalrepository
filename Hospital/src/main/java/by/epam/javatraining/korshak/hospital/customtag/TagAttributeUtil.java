package by.epam.javatraining.korshak.hospital.customtag;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import by.epam.javatraining.korshak.hospital.entity.Appointment;
import by.epam.javatraining.korshak.hospital.entity.AppointmentRecord;
import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.AppointmentService;
import by.epam.javatraining.korshak.hospital.service.PatientService;
import by.epam.javatraining.korshak.hospital.service.StaffService;
import by.epam.javatraining.korshak.hospital.service.impl.AppointmentServiceImpl;
import by.epam.javatraining.korshak.hospital.service.impl.PatientServiceImpl;
import by.epam.javatraining.korshak.hospital.service.impl.StaffServiceImpl;

public class TagAttributeUtil {
	public static final String NAME = "name";
	public static final String SURNAME = "surname";
	public static final String PATRONYMIC = "patronymic";
	public static final String ARRIVAL_DATE = "arrival_date";
	public static final String BIRTH_DATE = "date_of_birth";
	public static final String STATUS = "status";
	public static final String DEPARTMENT_ID = "department";
	public static final String WARD = "ward";
	public static final String DOCTOR_ID = "doctor";
	public static final String PATIENT_ID = "patient";
	private static final String DIAGNOSIS_ID = "diagnosis";
	private static final String APPOINTMENT_RECORD_ID = "appointmentRecord";
	private static final String APPOINTMENT_ID = "appointment";
	private static final String DATE_TIME = "date_time";
	private static final String PERFOMED_STAFF_ID = "perfomed_staff";
	private static final String DOSAGE = "dosage";
	
	private static String patientField(String field, Patient patient){
		switch(field){
		case NAME: 
			return patient.getName();
		case SURNAME:
			return patient.getSurname();
		case STATUS:
			return patient.getStatus().getPatientRole();
		case PATRONYMIC:
			return patient.getPatronymic();
		case ARRIVAL_DATE:
			return patient.getArrivalDate().toString();
		case BIRTH_DATE:
			return patient.getDateOfBirth().toString();
		case DEPARTMENT_ID:
			return ""+patient.getDepartmentId();
		case WARD:
			return ""+patient.getWard();
		case DOCTOR_ID:
			return ""+patient.getDoctorId();
		case PATIENT_ID:
			return ""+patient.getPatientId();
		default:
			return "";
	}
	}
	
	private static String diagnosisField(String field, Diagnosis diagnosis){
		switch(field){
		case NAME: 
			return diagnosis.getName();
		case DIAGNOSIS_ID:
			return ""+diagnosis.getDiagnosisId();
		default:
			return "";
		}
	}
	
	private static String appointmentsField(String field, AppointmentRecord appointment){
		PatientService patientService = new PatientServiceImpl();
		StaffService staffService = new StaffServiceImpl();
		AppointmentService appointmentService = new AppointmentServiceImpl();
		switch(field){
		case APPOINTMENT_RECORD_ID: 
			return ""+appointment.getAppointmentRecordId();
		case PATIENT_ID:
			Patient patient = null;
			try {
				patient = patientService.read(appointment.getPatientId());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(patient != null){
				return patient.getSurname() + " " + patient.getName(); 
			} else {
				return "";
			}
		case DOCTOR_ID:
			Staff doctor = null;
			try {
				doctor = staffService.read(appointment.getDoctorId());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(doctor != null){
				return doctor.getSurname() + " " + doctor.getName();
			} else {
				return "";
			}
		case APPOINTMENT_ID:
			Appointment appoint = null;
			try {
				appoint = appointmentService.read(appointment.getAppointmentId());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(appoint != null){
				return appoint.getName();
			} else {
				return "";
			}
		case DATE_TIME:
			DateFormat df = new SimpleDateFormat("dd.MM.yyyy hh:mm");
			return df.format(appointment.getDate());
		case PERFOMED_STAFF_ID:
			Staff staff = null;
			try {
				staff = staffService.read(appointment.getPerfomedStaffId());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(staff != null){
				return staff.getSurname() + " " + staff.getName();
			} else {
				return "";
			}
		case DOSAGE:
			return ""+appointment.getDosage();
		default:
			return "";
		}
	}
	
	public static String parseFieldValue(String field, Object object){
			if(object instanceof Patient){
				return patientField(field, (Patient)object);
			} else if(object instanceof Diagnosis){
				return diagnosisField(field, (Diagnosis)object);
			} else if(object instanceof AppointmentRecord) {
				return appointmentsField(field, (AppointmentRecord)object);
			}
			return "";
	}
}
