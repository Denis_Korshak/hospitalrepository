package by.epam.javatraining.korshak.hospital.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.PatientService;
import by.epam.javatraining.korshak.hospital.service.impl.PatientServiceImpl;

public class ShowTakeOutCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		PatientService patientService = new PatientServiceImpl();
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null || !user.isDoctor()){
			return ResourceManager.getProperty("page.index");
		}
		
		Patient patient = (Patient)request.getSession().getAttribute("patient");
		String selectedId = request.getParameter("selectedId");
		if(selectedId != null){
			patient = patientService.read(Integer.parseInt(selectedId));
			request.getSession().setAttribute("patient", patient);
		}
		if(null == patient){
			return (new ShowAllPatientsCommand()).execute(request);
		}
		
		List<Diagnosis> diagnoses = patientService.fetchDiagnosis(Integer.parseInt(selectedId));
		request.setAttribute("diagnoses", diagnoses);
		
		return ResourceManager.getProperty("page.take_out");
	}

}
