package by.epam.javatraining.korshak.hospital.service;

import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.Appointment;
import by.epam.javatraining.korshak.hospital.entity.role.AppointmentType;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public interface AppointmentService extends CommonService<Appointment> {

	int fetchIdOnName(String name) throws ServiceException;
	List<Appointment> fetchAppointmentsOnType(AppointmentType type) throws ServiceException;
	List<Appointment> fetchPatientAppointments(int patientId) throws ServiceException;
}
