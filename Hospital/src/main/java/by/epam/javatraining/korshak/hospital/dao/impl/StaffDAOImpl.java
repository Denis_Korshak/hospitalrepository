package by.epam.javatraining.korshak.hospital.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.StaffDAO;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.entity.role.StaffRole;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.poolconnections.PoolConnections;

public class StaffDAOImpl implements StaffDAO {

	private static final String SQL_CREATE_STAFF = 
			"INSERT INTO STAFF(surname, name, patronymic, role, id_department)"
			+ "VALUES(?,?,?,?,?)";
	
	private static final String SQL_FETCH_STAFF_ON_ID = 
			"SELECT staff.id_staff, staff.surname, staff.name, staff.patronymic, "
					+ "staff.role, staff.id_department from staff where staff.id_staff = ?";
	
	private static final String SQL_UPDATE_STAFF = 
			"UPDATE STAFF SET "
			+ "SURNAME = ?, "
			+ "NAME = ?, "
			+ "PATRONYMIC = ?, "
			+ "ROLE = ?, "
			+ "ID_DEPARTMENT = ? WHERE ID_STAFF = ?";
	
	private static final String SQL_DELETE_STAFF = 
			"DELETE STAFF WHERE ID_STAFF = ?";
	
	private static final String SQL_FETCH_STAFF = 
			"SELECT staff.id_staff, staff.surname, staff.name, staff.patronymic, "
			+ "staff.role, staff.id_department from staff";
	
	@Override
	public boolean create(Staff entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			System.out.println("POOOL" + pool);
			conn = pool.getConnection();
			System.out.println("conn" + conn);
			preparedStatement = conn.prepareStatement(SQL_CREATE_STAFF,Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, entity.getSurname());
			preparedStatement.setString(2, entity.getName());
			preparedStatement.setString(3, entity.getPatronymic());
			preparedStatement.setString(4, entity.getRole().getStaffRole());
			preparedStatement.setInt(5, entity.getDepartmentId());			
			preparedStatement.executeQuery();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			return resultSet.next();
		} catch(SQLException e){
			throw new DAOException("Fail insert into Staff.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public Staff fetch(int id) throws DAOException {
		Staff staff = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_STAFF_ON_ID);
			preparedStatement.setInt(1,id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				staff = getStaffFromResultSet(resultSet);
			}
		} catch(SQLException e){
			throw new DAOException("Fail insert into Staff.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return staff;
	}

	@Override
	public void update(Staff staff) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_UPDATE_STAFF);
			preparedStatement.setString(1, staff.getSurname());
			preparedStatement.setString(2, staff.getName());
			preparedStatement.setString(3, staff.getPatronymic());
			preparedStatement.setString(4, staff.getRole().getStaffRole());
			preparedStatement.setInt(5, staff.getDepartmentId());
			preparedStatement.setInt(6, staff.getStaffId());
			preparedStatement.executeQuery();
		} catch(SQLException e){
			throw new DAOException("Fail update Staff.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void delete(int id) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_DELETE_STAFF);
			preparedStatement.setInt(1, id);
			preparedStatement.executeQuery();
		} catch(SQLException e){
			throw new DAOException("Fail delete Staff.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}
	
	public void close(Statement st) throws DAOException {
		try{
			if(st != null){
				st.close();
			}
		} catch(SQLException e){
			throw new DAOException("Can't close statement.",e);
		}
	}

	@Override
	public List<Staff> fetchStaff() throws DAOException {
		List<Staff> result = new ArrayList<Staff>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_STAFF);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Staff staff = getStaffFromResultSet(resultSet);
				result.add(staff);
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetching Staff.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
				
		return result;
	}
	
	private Staff getStaffFromResultSet(ResultSet resultSet) throws SQLException{
		Staff staff = new Staff();
		staff.setStaffId(resultSet.getInt(1));
		staff.setSurname(resultSet.getString(2));
		staff.setName(resultSet.getString(3));
		staff.setPatronymic(resultSet.getString(4));
		staff.setRole(StaffRole.valueOf(resultSet.getString(5).toUpperCase()));
		staff.setDepartmentId(resultSet.getInt(6));
		return staff;
	}

}
