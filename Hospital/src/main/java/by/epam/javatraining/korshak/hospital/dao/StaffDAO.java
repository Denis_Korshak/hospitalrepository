package by.epam.javatraining.korshak.hospital.dao;

import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;

public interface StaffDAO extends CommonDAO<Staff> {

	List<Staff> fetchStaff() throws DAOException;
}
