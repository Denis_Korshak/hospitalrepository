package by.epam.javatraining.korshak.hospital.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.AppointmentDAO;
import by.epam.javatraining.korshak.hospital.entity.Appointment;
import by.epam.javatraining.korshak.hospital.entity.role.AppointmentType;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.poolconnections.PoolConnections;

public class AppointmentDAOImpl implements AppointmentDAO {

	private static final String SQL_CREATE_APPOINTMENT = 
			"INSERT INTO APPOINTMENTS_LIST(APPOINTMENT_TYPE, NAME) "
			+ "VALUES(?,?)";
	
	private static final String SQL_FETCH_APPOINTMENT_ON_ID = 
			"SELECT ID_APPOINTMENT_LIST, APPOINTMENT_TYPE, NAME "
			+ "from APPOINTMENTS_LIST where ID_APPOINTMENT_LIST = ?";
	
	private static final String SQL_UPDATE_APPOINTMENT = 
			"UPDATE APPOINTMENTS_LIST SET "
			+ "APPOINTMENT_TYPE = ?, "
			+ "NAME = ? WHERE ID_APPOINTMENT_LIST = ?";
	
	private static final String SQL_DELETE_APPOINTMENT = 
			"DELETE APPOINTMENTS_LIST WHERE ID_APPOINTMENT_LIST = ?";
	
	private static final String SQL_FETCH_APPOINTMENT_ON_NAME = 
			"SELECT ID_APPOINTMENT_LIST "
			+ "from APPOINTMENTS_LIST where NAME = ?";
	
	private static final String SQL_FETCH_APPOINTMENTS_ON_TYPE = 
			"SELECT ID_APPOINTMENT_LIST, APPOINTMENT_TYPE, NAME "
			+"FROM APPOINTMENTS_LIST WHERE APPOINTMENT_TYPE = ?";
	
	private static final String SQL_FETCH_PATIENT_APPOINTMENTS = 
			"select distinct al.id_appointment_list, al.appointment_type, al.name "
			+ "from appointments a, appointments_list al "
			+ "where al.id_appointment_list = a.id_appointment_list and " 
			+ "a.id_patient = ?";
	
	@Override
	public boolean create(Appointment entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_CREATE_APPOINTMENT,Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, entity.getType().getAppointmentType());
			preparedStatement.setString(2, entity.getName());			
			preparedStatement.executeQuery();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			return resultSet.next();
		} catch(SQLException e){
			throw new DAOException("Fail insert into Appointments.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public Appointment fetch(int id) throws DAOException {
		Appointment appointment = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_APPOINTMENT_ON_ID);
			preparedStatement.setInt(1,id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				appointment = getAppointmentFromResultSet(resultSet);
			}
		} catch(SQLException e){
			throw new DAOException("Fail insert into Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return appointment;
	}

	@Override
	public void update(Appointment entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_UPDATE_APPOINTMENT);
			preparedStatement.setString(1, entity.getType().getAppointmentType());
			preparedStatement.setString(2, entity.getName());
			preparedStatement.setInt(3, entity.getAppointmentId());
			preparedStatement.executeQuery();
		} catch(SQLException e){
			throw new DAOException("Fail update Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void delete(int id) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_DELETE_APPOINTMENT);
			preparedStatement.setInt(1, id);
			preparedStatement.executeQuery();
		} catch(SQLException e){
			throw new DAOException("Fail delete Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void close(Statement st) throws DAOException {
		try{
			if(st != null){
				st.close();
			}
		} catch(SQLException e){
			throw new DAOException("Can't close statement.",e);
		}
	}

	@Override
	public int fetchIdOnName(String name) throws DAOException {
		int appointmentId = -1;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_APPOINTMENT_ON_NAME);
			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				appointmentId = resultSet.getInt(1);
			}
		} catch(SQLException e){
			throw new DAOException("Fail insert into Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return appointmentId;
	}
	
	
	private Appointment getAppointmentFromResultSet(ResultSet resultSet) throws SQLException{
		Appointment appointment = new Appointment();
		appointment.setAppointmentId(resultSet.getInt(1));
		appointment.setType(AppointmentType.valueOf(resultSet.getString(2).toUpperCase()));
		appointment.setName(resultSet.getString(3));
		return appointment;
	}

	@Override
	public List<Appointment> fetchAppointmentsOnType(AppointmentType type)
			throws DAOException {
		List<Appointment> appointments = new ArrayList<Appointment>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_APPOINTMENTS_ON_TYPE);
			preparedStatement.setString(1, type.getAppointmentType());
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				appointments.add(getAppointmentFromResultSet(resultSet));
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return appointments;
	}

	@Override
	public List<Appointment> fetchPatientAppointments(int patientId)
			throws DAOException {
		List<Appointment> appointments = new ArrayList<Appointment>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_PATIENT_APPOINTMENTS);
			preparedStatement.setInt(1, patientId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				appointments.add(getAppointmentFromResultSet(resultSet));
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Appointment.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return appointments;
	}

}
