package by.epam.javatraining.korshak.hospital.command.utils;

import java.util.ResourceBundle;

public class ResourceManager {

	private final static ResourceBundle RESOURCE_BUNDLE = 
			ResourceBundle.getBundle("resources.config");

	private ResourceManager(){	}

	public static String getProperty(String key){
		return RESOURCE_BUNDLE.getString(key);
	}
}
