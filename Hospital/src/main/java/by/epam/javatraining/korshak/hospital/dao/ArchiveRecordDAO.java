package by.epam.javatraining.korshak.hospital.dao;

import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.ArchiveRecord;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;

public interface ArchiveRecordDAO extends CommonDAO<ArchiveRecord> {

	List<ArchiveRecord> fetchOnName(String surname, String name, String patronymic) throws DAOException;
	
}
