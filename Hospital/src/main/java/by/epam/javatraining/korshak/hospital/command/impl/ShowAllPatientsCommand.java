package by.epam.javatraining.korshak.hospital.command.impl;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.PatientService;
import by.epam.javatraining.korshak.hospital.service.impl.PatientServiceImpl;

public class ShowAllPatientsCommand implements ActionCommand {

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		PatientService service = new PatientServiceImpl();
		List<Patient> patients = null;
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null){
			return ResourceManager.getProperty("page.index");
		}
		if(user.isRegistry()){
			patients = service.fetchAllPatients();
		}
		if(user.isDoctor() || user.isNurse()){
			patients = service.fetchPatientsOnDepartment(user.getDepartmentId());
		}
		
		patients = (List<Patient>) (patients == null ? Collections.emptyList() : patients);
		request.setAttribute("patients", patients);
		
		return ResourceManager.getProperty("page.patients");
	}

}
