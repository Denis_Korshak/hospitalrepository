package by.epam.javatraining.korshak.hospital.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.ArchiveRecordDAO;
import by.epam.javatraining.korshak.hospital.dao.PatientDAO;
import by.epam.javatraining.korshak.hospital.dao.impl.ArchiveRecordDAOImpl;
import by.epam.javatraining.korshak.hospital.dao.impl.PatientDAOImpl;
import by.epam.javatraining.korshak.hospital.entity.ArchiveRecord;
import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.PatientService;

public class PatientServiceImpl implements PatientService {
	
	private PatientDAO patientDAO;
	
	public PatientServiceImpl(){
		this.patientDAO = new PatientDAOImpl();
	}
	
	@Override
	public boolean create(Patient entity) throws ServiceException {
		boolean result = false;
		try {
			result = patientDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail adding new patient.",e);
		}
		return result;
	}

	@Override
	public void update(Patient entity) throws ServiceException {
		try {
			patientDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail updating patient " + entity,e);
		}
		
	}

	@Override
	public Patient read(int id) throws ServiceException {
		Patient patient = null;
		try {
			patient = patientDAO.fetch(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading patient by id = " + id,e);
		}
		return patient;
	}

	@Override
	public void delete(int id) throws ServiceException {
		try {
			patientDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail deleting patient by id = " + id,e);
		}
	}

	@Override
	public List<Patient> fetchPatientsOnDepartment(int id)
			throws ServiceException {
		List<Patient> patients = new ArrayList<Patient>();
		try {
			patients = patientDAO.fetchPatientsOnDepartment(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail fetching patients on department = " + id,e);
		}
		return patients;
	}

	@Override
	public List<Patient> fetchAllPatients() throws ServiceException {
		List<Patient> patients = null;
		try {
			patients = patientDAO.fetchAllPatients();
		} catch (DAOException e) {
			throw new ServiceException("Fail fetching patients",e);
		}
		return patients;
	}

	@Override
	public List<Patient> fetchNewPatients(int departmentId) throws ServiceException {
		List<Patient> patients = null;
		try {
			patients = patientDAO.fetchNewPatients(departmentId);
		} catch (DAOException e) {
			throw new ServiceException("Fail fetching patients",e);
		}
		return patients;
	}

	@Override
	public List<Patient> searchOnSurname(String surname)
			throws ServiceException {
		List<Patient> patients = null;
		try {
			patients = patientDAO.searchOnSurname(surname);
		} catch (DAOException e) {
			throw new ServiceException("Fail fetching patients on surname = " + surname,e);
		}
		return patients;
	}

	@Override
	public void addDiagnosis(int patientId, int diagnosisId)
			throws ServiceException {
		try {
			patientDAO.addDiagnosis(patientId, diagnosisId);
		} catch (DAOException e) {
			throw new ServiceException("Fail adding diagnosis to patient." ,e);
		}		
	}

	@Override
	public List<Diagnosis> fetchDiagnosis(int patientId)
			throws ServiceException {
		List<Diagnosis> diagnoses = null;
		try {
			diagnoses = patientDAO.fetchDiagnosis(patientId);
		} catch (DAOException e) {
			throw new ServiceException("Fail fetching diagnoses of patient " + patientId,e);
		}
		return diagnoses;
	}

	@Override
	public void deleteDiagnosis(int patientId, int diagnosisId)
			throws ServiceException {
		try {
			patientDAO.deleteDiagnosis(patientId, diagnosisId);
		} catch (DAOException e) {
			throw new ServiceException("Fail deleting diagnosis of patient." ,e);
		}
	}

	@Override
	public void takePatientOut(Patient patient, int diagnosisId) throws ServiceException {
		ArchiveRecordDAO archiveRecordDAO = new ArchiveRecordDAOImpl();
		
		ArchiveRecord archiveRecord = new ArchiveRecord();
		archiveRecord.setDiagnosisId(diagnosisId);
		archiveRecord.setExitDate(new Date());
		archiveRecord.setName(patient.getName());
		archiveRecord.setSurname(patient.getSurname());
		archiveRecord.setPatronymic(patient.getPatronymic());
		try {
			archiveRecordDAO.create(archiveRecord);
		} catch (DAOException e) {
			throw new ServiceException("Fail creating new archive record", e);
		}
		try {
			patientDAO.delete(patient.getPatientId());
		} catch (DAOException e) {
			throw new ServiceException("Fail deleting patient " + patient, e);
		}
	}

}
