package by.epam.javatraining.korshak.hospital.entity.role;

public enum StaffRole {

	REGISTRY{
		{
			this.staffRole = "registry";
		}
	},
	DOCTOR{
		{
			this.staffRole = "doctor";
		}
	},
	HEADDOCTOR{
		{
			this.staffRole = "headdoctor";
		}
	},
	NURSE{
		{
			this.staffRole = "nurse";
		}
	};
	
	String staffRole;
	
	public String getStaffRole(){
		return staffRole;
	}
	
}
