package by.epam.javatraining.korshak.hospital.dao;

import java.util.Date;
import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.AppointmentRecord;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;

public interface AppointmentRecordDAO extends CommonDAO<AppointmentRecord> {

	List<AppointmentRecord> fetchAllOnDate(Date date) throws DAOException;
	List<AppointmentRecord> fetchPatientAppointments(int id) throws DAOException;
	void deleteAllAppointmentsOnId(int appointmentId) throws DAOException;
	List<AppointmentRecord> fetchOperationsOnToday(int departmentId) throws DAOException;
	List<AppointmentRecord> fetchAppointmentsOnToday(int departmentId) throws DAOException;
}
