package by.epam.javatraining.korshak.hospital.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public class LogoutCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		
		request.getSession().invalidate();
		return ResourceManager.getProperty("page.index");
		
	}

}
