package by.epam.javatraining.korshak.hospital.entity.role;

public enum AppointmentType {

	PROCEDURE{
		{
			appointmentType = "procedure";
		}
	},
	OPERATION{
		{
			appointmentType = "operation";
		}
	},
	MEDICATION{
		{
			appointmentType = "medication";
		}
	};
	
	String appointmentType;
	
	private AppointmentType(){}
	
	public String getAppointmentType(){
		return appointmentType;
	}
	
}
