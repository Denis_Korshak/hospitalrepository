package by.epam.javatraining.korshak.hospital.service.impl;

import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.DiagnosisDAO;
import by.epam.javatraining.korshak.hospital.dao.impl.DiagnosisDAOImpl;
import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.DiagnosisService;

public class DiagnosisServiceImpl implements DiagnosisService {

	private DiagnosisDAO diagnosisDAO;
	
	public DiagnosisServiceImpl() {
		diagnosisDAO = new DiagnosisDAOImpl();
	}
	
	@Override
	public boolean create(Diagnosis entity) throws ServiceException {
		boolean result = false;
		try {
			result = diagnosisDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail creation diagnosis " + entity,e);
		}
		return result;
	}

	@Override
	public void update(Diagnosis entity) throws ServiceException {
		try {
			diagnosisDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail updating diagnosis " + entity,e);
		}
	}

	@Override
	public Diagnosis read(int id) throws ServiceException {
		Diagnosis diagnosis = null;
		try {
			diagnosis = diagnosisDAO.fetch(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading diagnosis on id = " + id,e);
		}
		return diagnosis;
	}

	@Override
	public void delete(int id) throws ServiceException {
		try {
			diagnosisDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail deleting diagnosis on id = " + id,e);
		}
	}

	@Override
	public List<Diagnosis> fetchAllDiagnoses() throws ServiceException {
		List<Diagnosis> diagnoses = null;
		try {
			diagnoses = diagnosisDAO.fetchAllDiagnoses();
		} catch (DAOException e) {
			throw new ServiceException("Fail reading diagnoses",e);
		}
		return diagnoses;
	}

}
