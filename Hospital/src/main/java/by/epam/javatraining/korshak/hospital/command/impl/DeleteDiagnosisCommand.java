package by.epam.javatraining.korshak.hospital.command.impl;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.utils.ResourceManager;
import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.entity.Patient;
import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.DiagnosisService;
import by.epam.javatraining.korshak.hospital.service.PatientService;
import by.epam.javatraining.korshak.hospital.service.impl.DiagnosisServiceImpl;
import by.epam.javatraining.korshak.hospital.service.impl.PatientServiceImpl;

public class DeleteDiagnosisCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		
		PatientService patientService = new PatientServiceImpl();
		DiagnosisService diagnosisService = new DiagnosisServiceImpl();
		
		String diagnosisIdToDelete = request.getParameter("selectedId");
		
		Staff user = (Staff)request.getSession().getAttribute("user");
		if(user == null || !user.isDoctor()){
			return ResourceManager.getProperty("page.index");
		} 
		
		Patient patient = (Patient)request.getSession().getAttribute("patient");
		if(patient == null){
			return ResourceManager.getProperty("page.patients");
		}
		
		patientService.deleteDiagnosis(patient.getPatientId(), Integer.parseInt(diagnosisIdToDelete));
		
		List<Diagnosis> diagnoses = patientService.fetchDiagnosis(patient.getPatientId());
		diagnoses = (List<Diagnosis>) (diagnoses == null ? Collections.emptyList() : diagnoses);
		request.setAttribute("diagnoses", diagnoses);
			
		List<Diagnosis> allDiagnoses = diagnosisService.fetchAllDiagnoses();
		allDiagnoses = (List<Diagnosis>) (allDiagnoses == null ? Collections.emptyList() : allDiagnoses);
		request.setAttribute("allDiagnoses", allDiagnoses);
			
		return ResourceManager.getProperty("page.diagnoses");
	}

}
