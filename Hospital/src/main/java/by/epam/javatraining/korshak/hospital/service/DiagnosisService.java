package by.epam.javatraining.korshak.hospital.service;

import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public interface DiagnosisService extends CommonService<Diagnosis> {
	List<Diagnosis> fetchAllDiagnoses() throws ServiceException;
}
