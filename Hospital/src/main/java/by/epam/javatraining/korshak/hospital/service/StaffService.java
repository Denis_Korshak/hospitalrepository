package by.epam.javatraining.korshak.hospital.service;

import java.util.List;

import by.epam.javatraining.korshak.hospital.entity.Staff;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

public interface StaffService extends CommonService<Staff>{
	
	List<Staff> fetchStaff() throws ServiceException;
	
}
