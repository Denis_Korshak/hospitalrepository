package by.epam.javatraining.korshak.hospital.entity;

import java.util.Date;

public class ArchiveRecord {

	private int archiveRecordId;
	private String surname;
	private String name;
	private String patronymic;
	private Date exitDate;
	private int diagnosisId;
	
	public ArchiveRecord(){}

	public int getArchiveRecordId() {
		return archiveRecordId;
	}

	public void setArchiveRecordId(int archiveRecordId) {
		this.archiveRecordId = archiveRecordId;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public Date getExitDate() {
		return exitDate;
	}

	public void setExitDate(Date exitDate) {
		this.exitDate = exitDate;
	}

	public int getDiagnosisId() {
		return diagnosisId;
	}

	public void setDiagnosisId(int diagnosisId) {
		this.diagnosisId = diagnosisId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + archiveRecordId;
		result = prime * result + diagnosisId;
		result = prime * result
				+ ((exitDate == null) ? 0 : exitDate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((patronymic == null) ? 0 : patronymic.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArchiveRecord other = (ArchiveRecord) obj;
		if (archiveRecordId != other.archiveRecordId)
			return false;
		if (diagnosisId != other.diagnosisId)
			return false;
		if (exitDate == null) {
			if (other.exitDate != null)
				return false;
		} else if (!exitDate.equals(other.exitDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (patronymic == null) {
			if (other.patronymic != null)
				return false;
		} else if (!patronymic.equals(other.patronymic))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ArchiveRecord [archiveRecordId=" + archiveRecordId
				+ ", surname=" + surname + ", name=" + name + ", patronymic="
				+ patronymic + ", exitDate=" + exitDate + ", diagnosisId="
				+ diagnosisId + "]";
	}
	
	
}
