package by.epam.javatraining.korshak.hospital.service.impl;

import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.AppointmentDAO;
import by.epam.javatraining.korshak.hospital.dao.impl.AppointmentDAOImpl;
import by.epam.javatraining.korshak.hospital.entity.Appointment;
import by.epam.javatraining.korshak.hospital.entity.role.AppointmentType;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;
import by.epam.javatraining.korshak.hospital.service.AppointmentService;

public class AppointmentServiceImpl implements AppointmentService {

	private AppointmentDAO appointmentDAO;
	
	public AppointmentServiceImpl(){
		appointmentDAO = new AppointmentDAOImpl();
	}
	
	@Override
	public boolean create(Appointment entity) throws ServiceException {
		boolean result = false;
		try {
			result = appointmentDAO.create(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail creating appointment "+entity,e);
		}
		return result;
	}

	@Override
	public void update(Appointment entity) throws ServiceException {
		try {
			appointmentDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException("Fail updating appointment "+entity,e);
		}
	}

	@Override
	public Appointment read(int id) throws ServiceException {
		Appointment appointment = null;
		try {
			appointment = appointmentDAO.fetch(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading appointment on id = "+id,e);
		}
		return appointment;
	}

	@Override
	public void delete(int id) throws ServiceException {
		try {
			appointmentDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("Fail deleting appointment on id = "+id,e);
		}
	}

	@Override
	public int fetchIdOnName(String name) throws ServiceException {
		int id = 0;
		try {
			id = appointmentDAO.fetchIdOnName(name);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading appointment on name = "+name,e);
		}
		return id;
	}

	@Override
	public List<Appointment> fetchAppointmentsOnType(AppointmentType type)
			throws ServiceException {
		List<Appointment> appointments = null;
		try {
			appointments = appointmentDAO.fetchAppointmentsOnType(type);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading appointments on type = "+type,e);
		}
		return appointments;
	}

	@Override
	public List<Appointment> fetchPatientAppointments(int patientId)
			throws ServiceException {
		List<Appointment> appointments = null;
		try {
			appointments = appointmentDAO.fetchPatientAppointments(patientId);
		} catch (DAOException e) {
			throw new ServiceException("Fail reading appointments on patientId = "+patientId,e);
		}
		return appointments;
	}

}
