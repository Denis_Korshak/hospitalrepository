package by.epam.javatraining.korshak.hospital.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.javatraining.korshak.hospital.command.ActionCommand;
import by.epam.javatraining.korshak.hospital.command.fatory.ActionFactory;
import by.epam.javatraining.korshak.hospital.exeption.ServiceException;

@WebServlet("/controller")
public class MainServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public MainServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		ActionFactory actionFactory = new ActionFactory();
		ActionCommand command = actionFactory.defineCommand(request);
		String page = null;
		try {
			page = command.execute(request);
		} catch (ServiceException e) {
			//!!!!!!!!!!!!!!!!!
		}
		if(page != null){
			//response.sendRedirect(page);
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

}
