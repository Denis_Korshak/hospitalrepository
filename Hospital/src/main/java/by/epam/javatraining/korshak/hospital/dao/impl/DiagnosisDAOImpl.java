package by.epam.javatraining.korshak.hospital.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.epam.javatraining.korshak.hospital.dao.DiagnosisDAO;
import by.epam.javatraining.korshak.hospital.entity.Diagnosis;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.poolconnections.PoolConnections;

public class DiagnosisDAOImpl implements DiagnosisDAO {

	private static final String SQL_CREATE_DIAGNOSIS = 
			"INSERT INTO DIAGNOSES(NAME) VALUES(?)";
	
	private static final String SQL_FETCH_DIAGNOSIS_ON_ID = 
			"SELECT ID_DIAGNOSIS, NAME from DIAGNOSES where ID_DIAGNOSIS = ?";
	
	private static final String SQL_UPDATE_DIAGNOSIS	 = 
			"UPDATE DIAGNOSES SET NAME = ? WHERE ID_DIAGNOSIS = ?";
	
	private static final String SQL_DELETE_DIAGNOSIS = 
			"DELETE DIAGNOSES WHERE ID_DIAGNOSIS = ?";
	
	private static final String SQL_FETCH_ALL_DIAGNOSES = 
			"SELECT ID_DIAGNOSIS, NAME from DIAGNOSES";
	
	@Override
	public boolean create(Diagnosis entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_CREATE_DIAGNOSIS,Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, entity.getName());
			preparedStatement.executeQuery();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			return resultSet.next();
		} catch(SQLException e){
			throw new DAOException("Fail insert into Diagnosis.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public Diagnosis fetch(int id) throws DAOException {
		Diagnosis diagnosis = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_DIAGNOSIS_ON_ID);
			preparedStatement.setInt(1,id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				diagnosis = new Diagnosis();
				diagnosis.setDiagnosisId(resultSet.getInt(1));
				diagnosis.setName(resultSet.getString(2));
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Diagnosis.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return diagnosis;
	}

	@Override
	public void update(Diagnosis entity) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_UPDATE_DIAGNOSIS);
			preparedStatement.setString(1, entity.getName());
			preparedStatement.setInt(2, entity.getDiagnosisId());		
			preparedStatement.executeQuery();
		} catch(SQLException e){
			throw new DAOException("Fail update Diagnosis.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void delete(int id) throws DAOException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_DELETE_DIAGNOSIS);
			preparedStatement.setInt(1, id);
			preparedStatement.executeQuery();
		} catch(SQLException e){
			throw new DAOException("Fail delete Diagnosis.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
	}

	@Override
	public void close(Statement st) throws DAOException {
		try{
			if(st != null){
				st.close();
			}
		} catch(SQLException e){
			throw new DAOException("Can't close statement.",e);
		}
	}

	@Override
	public List<Diagnosis> fetchAllDiagnoses() throws DAOException {
		List<Diagnosis> diagnoses = new ArrayList<Diagnosis>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		PoolConnections pool = null;
		try {
			pool = PoolConnections.getInstance();
			conn = pool.getConnection();
			preparedStatement = conn.prepareStatement(SQL_FETCH_ALL_DIAGNOSES);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				Diagnosis diagnosis = new Diagnosis();
				diagnosis.setDiagnosisId(resultSet.getInt(1));
				diagnosis.setName(resultSet.getString(2));
				diagnoses.add(diagnosis);
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Diagnosis.",e);
		} finally {
			close(preparedStatement);
			pool.returnResource(conn);
		}
		return diagnoses;
	}

}
