package by.epam.javatraining.korshak.hospital.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import by.epam.javatraining.korshak.hospital.dao.DepartmentDAO;
import by.epam.javatraining.korshak.hospital.entity.Department;
import by.epam.javatraining.korshak.hospital.exeption.DAOException;
import by.epam.javatraining.korshak.hospital.poolconnections.PoolConnections;

public class DepartmentDAOImpl implements DepartmentDAO {

	private static final String SQL_CREATE_DEPARTMENT = 
			"INSERT INTO DEPARTMENTS(NAME, ID_MAIN_DOCTOR) VALUES(?,?)";
	
	private static final String SQL_FETCH_DEPARTMENT_ON_ID = 
			"SELECT ID_DEPARTMENT, NAME, ID_MAIN_DOCTOR from DEPARTMENTS where ID_DEPARTMENT = ?";
	
	private static final String SQL_UPDATE_DEPARTMENT	 = 
			"UPDATE DEPARTMENTS SET NAME = ?, ID_MAIN_DOCTOR = ? WHERE ID_DEPARTMENT = ?";
	
	private static final String SQL_DELETE_DEPARTMENT = 
			"DELETE DEPARTMENTS WHERE ID_DEPARTMENT = ?";
	
	private static final String SQL_FETCH_DEPARTMENT_ON_NAME = 
			"SELECT ID_DEPARTMENT, NAME, ID_MAIN_DOCTOR from DEPARTMENTS where NAME = ?";
	
	@Override
	public boolean create(Department entity) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		PoolConnections poolConnection = null;
		try {
			poolConnection = PoolConnections.getInstance();
			connection = poolConnection.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE_DEPARTMENT,Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, entity.getName());
			preparedStatement.setInt(2, entity.getMainDoctorId());
			preparedStatement.executeQuery();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			return resultSet.next();
		} catch(SQLException e){
			throw new DAOException("Fail insert into Departments.",e);
		} finally {
			close(preparedStatement);
			poolConnection.returnResource(connection);
		}
	}

	@Override
	public Department fetch(int id) throws DAOException {
		Department department = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		PoolConnections poolConnection = null;
		try {
			poolConnection = PoolConnections.getInstance();
			connection = poolConnection.getConnection();
			preparedStatement = connection.prepareStatement(SQL_FETCH_DEPARTMENT_ON_ID);
			preparedStatement.setInt(1,id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				department = new Department();
				department.setDepartmentId(resultSet.getInt(1));
				department.setName(resultSet.getString(2));
				department.setMainDoctorId(resultSet.getInt(3));
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Departments.",e);
		} finally {
			close(preparedStatement);
			poolConnection.returnResource(connection);
		}
		return department;
	}

	@Override
	public void update(Department entity) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		PoolConnections poolConnection = null;
		try {
			poolConnection = PoolConnections.getInstance();
			connection = poolConnection.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_DEPARTMENT);
			preparedStatement.setString(1, entity.getName());
			preparedStatement.setInt(2, entity.getMainDoctorId());
			preparedStatement.setInt(3, entity.getDepartmentId());		
			preparedStatement.executeQuery();
		} catch(SQLException e){
			throw new DAOException("Fail update Departments.",e);
		} finally {
			close(preparedStatement);
			poolConnection.returnResource(connection);
		}
	}

	@Override
	public void delete(int id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		PoolConnections poolConnection = null;
		try {
			poolConnection = PoolConnections.getInstance();
			connection = poolConnection.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_DEPARTMENT);
			preparedStatement.setInt(1, id);
			preparedStatement.executeQuery();
		} catch(SQLException e){
			throw new DAOException("Fail delete Departments.",e);
		} finally {
			close(preparedStatement);
			poolConnection.returnResource(connection);
		}
	}

	@Override
	public void close(Statement st) throws DAOException {
		try{
			if(st != null){
				st.close();
			}
		} catch(SQLException e){
			throw new DAOException("Can't close statement.",e);
		}
	}

	@Override
	public Department fetchDepartmentOnName(String name) throws DAOException {
		Department department = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		PoolConnections poolConnection = null;
		try {
			poolConnection = PoolConnections.getInstance();
			connection = poolConnection.getConnection();
			preparedStatement = connection.prepareStatement(SQL_FETCH_DEPARTMENT_ON_NAME);
			preparedStatement.setString(1,name);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				department = new Department();
				department.setDepartmentId(resultSet.getInt(1));
				department.setName(resultSet.getString(2));
				department.setMainDoctorId(resultSet.getInt(3));
			}
		} catch(SQLException e){
			throw new DAOException("Fail fetch from Departments.",e);
		} finally {
			close(preparedStatement);
			poolConnection.returnResource(connection);
		}
		return department;
	}

}
