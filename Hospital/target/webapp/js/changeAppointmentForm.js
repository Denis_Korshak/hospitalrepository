$(document).ready(function() {
	$.urlParam = function(name){
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
	};
	
	$('#form_selector').change(function(event) {
		var appointmenttype = $(this).val();
		var command = 'change_form';
		$('#addAppointment').load('controller',{command:command,appointmenttype:appointmenttype});
	});
});

