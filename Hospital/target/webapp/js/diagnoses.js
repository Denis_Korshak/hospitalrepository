$(document).ready(function() {

	$.urlParam = function(name){
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
	};
	
	$('#add_diagnosis').click(function(event) {
		var command = "add_diagnosis";
		var diagnosisToAdd = $('select[name="diagnosisToAdd"]').val();
		$.post('controller',{command:command,diagnosisToAdd:diagnosisToAdd},function(data){
	        $('body').html(data);
		});
	});
	
	$('#delete_diagnosis').click(function(event) {
		var command = "delete_diagnosis";
		var selectedId = $('input[name="selectedId"]').val();
		$.post('controller',{command:command,selectedId:selectedId},function(data){
	        $('body').html(data);
		});
	});
});

