$(document).ready(function() {
	$('#locale_selector').change(function(event) {
		var localeSelected = $(this).val();
		var command = "change_locale";
		$.post('controller',{command:command,localeSelected:localeSelected});
		location.reload();
	});
});

