$(document).ready(function() {

	$.urlParam = function(name){
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
	};
	
	$('#delete_button').click(function(event) {
		var command = "delete_appointment";
		var appointmentToDelete = $('select[name="appointmentToDelete"]').val();
		$.post('controller',{command:command,appointmentToDelete:appointmentToDelete},function(data){
	        $('body').html(data);
		});
	});
	
	$('#perfome-appointment').click(function(event) {
		$.post('controller',$('#perfome-form').serialize(),function(data){
	        $('body').html(data);
	    });
	});
	
	$('#submit').click(function(event) {
		$.post('controller',$('.appointment-form').serialize(),function(data){
	        $('body').html(data);
	    });
	});
	
	$('#button-registrate').click(function(event) {
		$.post('controller',$('#registration-form').serialize(),function(data){
	        $('body').html(data);
	    });
	});
	
	$('#add_diagnosis').click(function(event) {
		var command = "add_diagnosis";
		var diagnosisToAdd = $('select[name="diagnosisToAdd"]').val();
		$.post('controller',{command:command,diagnosisToAdd:diagnosisToAdd},function(data){
	        $('body').html(data);
		});
	});
	
	$('#delete_diagnosis').click(function(event) {
		var command = "delete_diagnosis";
		var selectedId = $('input[name="selectedId"]').val();
		$.post('controller',{command:command,selectedId:selectedId},function(data){
	        $('body').html(data);
		});
	});
	
	$('#button-take-out').click(function(event) {
		$.post('controller',$('#take-out-form').serialize(),function(data){
	        $('body').html(data);
	    });
	});
});

