$(document).ready(function() {

	$.urlParam = function(name){
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
	};
	
	$('#delete_button').click(function(event) {
		var command = "delete_appointment";
		var appointmentToDelete = $('select[name="appointmentToDelete"]').val();
		$.post('controller',{command:command,appointmentToDelete:appointmentToDelete},function(data){
	        $('body').html(data);
		});
	});
	
	$('#perfome-appointment').click(function(event) {
		$.post('controller',$('#perfome-form').serialize(),function(data){
	        $('body').html(data);
	    });
	});
	
	$('#submit').click(function(event) {
		$.post('controller',$('.appointment-form').serialize(),function(data){
	        $('body').html(data);
	    });
	});
});

