<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/changeRowsCount.js"></script>
	<script src="${pageContext.request.contextPath}/js/fetchSelectedId.js"></script>
	<script src="${pageContext.request.contextPath}/js/diagnoses.js"></script>
			
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	
</head>
<body>

	<c:import url="/jsp/fragment/header.jsp"></c:import>
	
	<c:choose> 
  		<c:when test="${empty param.rowsperpage}">
    		<c:set var="rowsperpage" value="10"/>
  		</c:when>
  		<c:otherwise>
    		<c:set var="rowsperpage" value="${param.rowsperpage}"/>
  		</c:otherwise>
	</c:choose>
	<c:choose> 
  		<c:when test="${empty param.currentpage}">
    		<c:set var="currentpage" value="1"/>
  		</c:when>
  		<c:otherwise>
    		<c:set var="currentpage" value="${param.currentpage}"/>
  		</c:otherwise>
	</c:choose>
	
	
	<div class="container">
		<div class="row">
		<blockquote class="blockquote-reverse">
  			<label class="control-label">Диагнозы ${patient.surname } ${patient.name }</label>
		</blockquote>
		</div>
		<div class="row">
			<form class="form-inline" action="controller" method="post">
				<input type="hidden" name="command" value="add_diagnosis">
				<input type="hidden" name="patientId" value="${patient.patientId }">
				<select name="diagnosisToAdd"  class="form-control">
					<c:forEach items="${allDiagnoses}" var="diagnosis">
						<option value="${diagnosis.diagnosisId}">${diagnosis.name }</option>
					</c:forEach>
				</select>
				<input id="add_diagnosis" type="button" class="btn btn-primary" value="Add diagnosis">
			</form>
			
			<div id="buttons" style="display:none;">
				<form action="controller" method="post">
					<input type="hidden" name="command" value="delete_diagnosis">
					<input type="hidden" name="selectedId">
					<input id="delete_diagnosis" type="button" class="btn btn-danger" value="Delete diagnosis">
				</form>
			</div>
			
			<select id="rows_selector" class="form-control" name="rowsperpage">
		       	<option value="5" ${5 == rowsperpage ? 'selected="selected"' : ''}>5</option>
		       	<option value="10" ${10 == rowsperpage ? 'selected="selected"' : ''}>10</option>
		       	<option value="15" ${15 == rowsperpage ? 'selected="selected"' : ''}>15</option>
		       	<option value="20" ${20 == rowsperpage ? 'selected="selected"' : ''}>20</option>
			</select>
			
			<ctg:table collection="${diagnoses }" rowsPerPage="${rowsperpage }" currentPage="${currentpage}">
				<ctg:column head="id" field="diagnosis"></ctg:column>
				<ctg:column head="name" field="name"></ctg:column>
			</ctg:table>
		</div>
	</div>
	
</body>
</html>