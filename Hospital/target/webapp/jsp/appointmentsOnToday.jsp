<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/changeRowsCount.js"></script>
	<script src="${pageContext.request.contextPath}/js/fetchSelectedId.js"></script>
	<script src="${pageContext.request.contextPath}/js/appointments.js"></script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	
</head>
<body>
	
	<c:import url="/jsp/fragment/header.jsp"></c:import>

	<c:choose> 
  		<c:when test="${empty param.rowsperpage}">
    		<c:set var="rowsperpage" value="10"/>
  		</c:when>
  		<c:otherwise>
    		<c:set var="rowsperpage" value="${param.rowsperpage}"/>
  		</c:otherwise>
	</c:choose>
	<c:choose> 
  		<c:when test="${empty param.currentpage}">
    		<c:set var="currentpage" value="1"/>
  		</c:when>
  		<c:otherwise>
    		<c:set var="currentpage" value="${param.currentpage}"/>
  		</c:otherwise>
	</c:choose>
	
	<div class="container">
		<div class="row">
			<div id="buttons" style="display:none;">
				<form id="perfome-form" action="controller" method="post">
					<input type="hidden" name="command" value="perfome_appointment">
					<input type="hidden" name="selectedId">
					<input id="perfome-appointment" type="button" class="btn btn-primary" value="Perfome">
				</form>
			</div>
		
			<select id="rows_selector" class="form-control" name="rowsperpage">
		       	<option value="5" ${5 == rowsperpage ? 'selected="selected"' : ''}>5</option>
		       	<option value="10" ${10 == rowsperpage ? 'selected="selected"' : ''}>10</option>
		       	<option value="15" ${15 == rowsperpage ? 'selected="selected"' : ''}>15</option>
		       	<option value="20" ${20 == rowsperpage ? 'selected="selected"' : ''}>20</option>
			</select>
			
			<ctg:table collection="${appointmentRecords }" rowsPerPage="${rowsperpage }" currentPage="${currentpage}">
				<ctg:column head="id" field="appointmentRecord"></ctg:column>
				<ctg:column head="patient" field="patient"></ctg:column>
				<ctg:column head="doctor" field="doctor"></ctg:column>
				<ctg:column head="appointment" field="appointment"></ctg:column>
				<ctg:column head="datetime" field="date_time"></ctg:column>
				<ctg:column head="dosage" field="dosage"></ctg:column>
			</ctg:table>
		</div>
	</div>
	
</body>
</html>