<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/changeLocale.js"></script>
	<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet"/>
	<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet"/>
	<script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
	<script src="${pageContext.request.contextPath}/js/post-action.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-offset-11">
				<c:if test="${locale == null}">
					<c:set var="locale" value="ru_RU" scope="session" />
				</c:if>
				<select id="locale_selector" name="locale" class="form-control">
					<option value="en_US" ${'en_US' == locale ? 'selected="selected"' : ''}>ENG</option>
					<option value="ru_RU" ${'ru_RU' == locale ? 'selected="selected"' : ''}>RUS</option>
				</select>
				<fmt:setLocale value="${locale }" scope="session" />
				<fmt:setBundle basename="resources.pagecontent" var="rb"/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-9">
				<h1>Больница</h1>
			</div>

			<div class="col-sm-3" id="logout-form">
				<form action="controller" method="post" class="form-inline">
					<input type="hidden" name="command" value="log_out">
					<label class="control-label"><fmt:message key="label.welcome" bundle="${rb}" />, ${user.name }</label>
					<input type="submit" class="btn btn-default" value="Log out">
				</form>
			</div>
		</div>
		<div class="row">
			<div class="navbar navbar-default">
				<ul class="nav navbar-nav">
					<c:if test="${user.registry }">
						<li class="menu-item">
							<a href="controller?command=link&page=page.registrate">Регистрация</a>
						</li>
					</c:if>
					
					<c:if test="${user != null }">
						<li class="menu-item">
							<a href="controller?command=patients">Пациенты</a>
						</li>
					</c:if>
					
					<c:if test="${user.doctor }">
						<li class="menu-item">
							<a href="controller?command=new_patients">Поступившие пациенты</a>
						</li>
					</c:if>
					
					<c:if test="${user.doctor || user.nurse }">
						<li class="menu-item">
							<a href="controller?command=appointments_on_today">Назначения на сегодня</a>
						</li>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>