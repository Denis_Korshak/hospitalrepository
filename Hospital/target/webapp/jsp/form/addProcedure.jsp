<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${pageContext.request.contextPath}/js/changeAppointmentForm.js"></script>
	<script src="${pageContext.request.contextPath}/js/addTimeInput.js"></script>
	<script src="${pageContext.request.contextPath}/js/appointments.js"></script>
</head>
<body>
	<form action="controller"  method="post" class="form-horizontal">
		<input type="hidden" name="command" value="add_appointment">
		<div class="form-group">
			<label class="col-sm-2 ">Тип назначения:</label>
			<div class="col-sm-4">
				<select id="form_selector" name="appointmentType" class="form-control">
					<option value="medication">medication</option>
					<option value="procedure" selected="selected">procedure</option>
					<option value="operation">operation</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 ">Название:</label>
			<div class="col-sm-4">
				<select name="appointmentId" class="form-control">
					<c:forEach items="${appointmentList}" var="list">
						<option value="${list.appointmentId}">${list.name}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 ">Дата:</label>
			<div class="col-sm-4">
				<input type="text" name="dateTo" class="form-control" placeholder="31.12.2015" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 ">Время:</label>
			<div class="col-sm-2">
				<input type="text" name="time" value="09:00" class="form-control">
			</div>
			<div class="col-sm-2">
				<input type="button" id="addTime" value="Add time" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-4">
				<input id="submit" type="button" value="Добавить процедуру" class="btn btn-primary">
			</div>
		</div>
	</form>
</body>
</html>