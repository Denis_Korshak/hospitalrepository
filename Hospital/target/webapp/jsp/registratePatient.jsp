<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/registrate.js"></script>
	<title>Insert title here</title>
</head>
<body>
	<c:import url="/jsp/fragment/header.jsp"></c:import>
	<div class="container">
		<div class="row">
			<form id="registration-form" action="controller" method="post" class="form-horizontal">
				<input type="hidden" name="command" value="registrate_patient">
				<div class="form-group">
					<label class="col-sm-offset-2 col-sm-2 control-label">Фамилия</label>
					<div class="col-sm-6">
						<input type="text" name="surname" class="form-control" placeholder="Фамилия">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-offset-2 col-sm-2 control-label">Имя</label>
					<div class="col-sm-6">
						<input type="text" name="name" class="form-control" placeholder="Имя">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-offset-2 col-sm-2 control-label">Отчество</label>
					<div class="col-sm-6">
						<input type="text" name="patronymic" class="form-control" placeholder="Отчество">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-offset-2 col-sm-2 control-label">Дата рождения</label>
					<div class="col-sm-6">
						<input type="text" name="dateOfBirth" class="form-control" placeholder="Дата рождения">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-offset-2 col-sm-2 control-label">Номер отделения</label>
					<div class="col-sm-6">
						<input type="text" name="department" class="form-control" placeholder="Номер отделения">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-offset-2 col-sm-2 control-label">Палата</label>
					<div class="col-sm-6">
						<input type="text" name="ward" class="form-control" placeholder="Палата">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-4">
						<input id="button-registrate" type="button" class="btn btn-primary" value="Зарегистрировать">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>