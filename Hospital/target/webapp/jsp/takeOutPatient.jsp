<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/changeRowsCount.js"></script>
	<script src="${pageContext.request.contextPath}/js/fetchSelectedId.js"></script>
			
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	
</head>
<body>
	<c:import url="/jsp/fragment/header.jsp"></c:import>
	<div class="container">
		<div class="row">
			<blockquote class="blockquote-reverse">
  				<label class="control-label">Пациент ${patient.surname } ${patient.name }</label>
			</blockquote>
		</div>
		<div class="row">
			<form id="take-out-form" class="form-horizontal">
				<input type="hidden" name="command" value="take_out">
				<div class="form-group">
					<label class="col-sm-4 control-label">Окончательный диагноз</label>
					
					<div class="col-sm-3">
						<select name="diagnosisId" class="form-control">
							<c:forEach items="${diagnoses}" var="diagnosis">
								<option value="${diagnosis.diagnosisId}">${diagnosis.name }</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-sm-3">
						<input id="button-take-out" type="button" class="btn btn-primary" value="Take out">
					</div>
				</div>				
			</form>
		</div>
	</div>
	
	