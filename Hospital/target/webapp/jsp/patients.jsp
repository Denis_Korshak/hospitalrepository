<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/changeRowsCount.js"></script>
	<script src="${pageContext.request.contextPath}/js/fetchSelectedId.js"></script>
	<script src="${pageContext.request.contextPath}/js/tableRowClick.js"></script>
			
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	
</head>
<body>
	<c:import url="/jsp/fragment/header.jsp"></c:import>
	
	 <c:choose> 
  		<c:when test="${empty param.rowsperpage}">
    		<c:set var="rowsperpage" value="10"/>
  		</c:when>
  		<c:otherwise>
    		<c:set var="rowsperpage" value="${param.rowsperpage}"/>
  		</c:otherwise>
	</c:choose>
	<c:choose> 
  		<c:when test="${empty param.currentpage}">
    		<c:set var="currentpage" value="1"/>
  		</c:when>
  		<c:otherwise>
    		<c:set var="currentpage" value="${param.currentpage}"/>
  		</c:otherwise>
	</c:choose>
	
	<div class="container">
		<div class="row">

			<form id="search-form" class="form-inline">
				<input type="hidden" name="command" value="search_patient">
				<p>
					<input type="text" class="form-control" placeholder="Surname" name="surname">
					<input type="submit" class="btn btn-default" value="Search">
				</p>
			</form>
		</div>
		<div class="row">
			<div id="buttons" style="display:none;">		
				<form>
					<input type="hidden" name="command" value="patient_diagnoses">
					<input type="hidden" name="selectedId">
					<input type="submit" class="btn btn-primary" value="Diagnoses">
				</form>
				
				<form>
					<input type="hidden" name="command" value="patient_appointments">
					<input type="hidden" name="selectedId">
					<input type="submit" class="btn btn-primary" value="Appointments">
				</form>
				
				<form>
					<input type="hidden" name="command" value="finish_inspection">
					<input type="hidden" name="selectedId">
					<input type="submit" class="btn btn-primary" value="Finish inspection">
				</form>
				
				<form class="last">
					<input type="hidden" name="command" value="take_out_page">
					<input type="hidden" name="selectedId">
					<input type="submit" class="btn btn-danger" value="Take out of hospital">
				</form>
				
			</div>

			<select id="rows_selector" class="form-control" name="rowsperpage">
			  	<option value="5" ${5 == rowsperpage ? 'selected="selected"' : ''}>5</option>
			  	<option value="10" ${10 == rowsperpage ? 'selected="selected"' : ''}>10</option>
			   	<option value="15" ${15 == rowsperpage ? 'selected="selected"' : ''}>15</option>
			   	<option value="20" ${20 == rowsperpage ? 'selected="selected"' : ''}>20</option>
			</select>
			
			<ctg:table collection="${patients }" rowsPerPage="${rowsperpage }" currentPage="${currentpage}">
				<ctg:column head="id" field="patient"></ctg:column>
				<ctg:column head="surname" field="surname"></ctg:column>
				<ctg:column head="name" field="name"></ctg:column>
				<ctg:column head="patronymic" field="patronymic"></ctg:column>
				<ctg:column head="arrival date" field="arrival_date"></ctg:column>
				<ctg:column head="DoB" field="date_of_birth"></ctg:column>
				<ctg:column head="status" field="status"></ctg:column>
				<ctg:column head="department" field="department"></ctg:column>
				<ctg:column head="ward" field="ward"></ctg:column>
				<ctg:column head="doctor" field="doctor"></ctg:column>
			</ctg:table>
		</div>
	</div>
</body>
</html>