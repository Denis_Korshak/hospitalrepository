<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/changeRowsCount.js"></script>
	<script src="${pageContext.request.contextPath}/js/fetchSelectedId.js"></script>
			
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	
</head>
<body>

	<c:import url="/jsp/fragment/header.jsp"></c:import>
	
	<c:choose> 
  		<c:when test="${empty param.rowsperpage}">
    		<c:set var="rowsperpage" value="10"/>
  		</c:when>
  		<c:otherwise>
    		<c:set var="rowsperpage" value="${param.rowsperpage}"/>
  		</c:otherwise>
	</c:choose>
	<c:choose> 
  		<c:when test="${empty param.currentpage}">
    		<c:set var="currentpage" value="1"/>
  		</c:when>
  		<c:otherwise>
    		<c:set var="currentpage" value="${param.currentpage}"/>
  		</c:otherwise>
	</c:choose>
	
	
	<div class="container">
		<div class="row">
		<blockquote class="blockquote-reverse">
  			<label class="control-label">Назначения ${patient.surname } ${patient.name }</label>
		</blockquote>
			
			<div id="addAppointment" class="row">
				<c:import url="/jsp/form/addMedication.jsp"></c:import>
			</div>
			
			<form class="form-inline">
				<input type="hidden" name="command" value="delete_appointment">
				<select name="appointmentToDelete" class="form-control">
					<c:forEach items="${appointmentsToDelete}" var="appointment">
						<option value="${appointment.appointmentId }">${appointment.name }</option>
					</c:forEach>
				</select>
				<input id="delete_button" type="button" class="btn btn-danger" value="Delete appointment">
			</form>
			
			<select id="rows_selector" class="form-control" name="rowsperpage">
		       	<option value="5" ${5 == rowsperpage ? 'selected="selected"' : ''}>5</option>
		       	<option value="10" ${10 == rowsperpage ? 'selected="selected"' : ''}>10</option>
		       	<option value="15" ${15 == rowsperpage ? 'selected="selected"' : ''}>15</option>
		       	<option value="20" ${20 == rowsperpage ? 'selected="selected"' : ''}>20</option>
			</select>
			
			<ctg:table collection="${appointmentRecords }" rowsPerPage="${rowsperpage }" currentPage="${currentpage}">
				<ctg:column head="id" field="appointmentRecord"></ctg:column>
				<ctg:column head="patient" field="patient"></ctg:column>
				<ctg:column head="doctor" field="doctor"></ctg:column>
				<ctg:column head="appointment" field="appointment"></ctg:column>
				<ctg:column head="datetime" field="date_time"></ctg:column>
				<ctg:column head="dosage" field="dosage"></ctg:column>
				<ctg:column head="perfomed_staff" field="perfomed_staff"></ctg:column>
			</ctg:table>
		</div>
	</div>
	
</body>
</html>